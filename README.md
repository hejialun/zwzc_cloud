# 基于springcloud+nacos+vue 实现的分布式RBAC权限系统

#### Description
基于springcloud+nacos+vue 实现的分布式RBAC权限系统（zwzc_cloud）,框架内容比较基础，没有过多的冗余内容,
实现了前后端的用户和权限模块的相关内容，框架还不够完善用于学习使用，有问题欢迎大家指出，相互学习

### 内置基础功能
1. 登录
2. 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
3. 角色管理：用户角色管理，为每一个角色设置不同的菜单权限已经不同的数据权限规则
4. 用户管理: 用户实现，多角色实现。
5. 字典管理：字典表维护。
6. 部门管理：管理用户所在部门，并实现数据权限，通过mapper接口配置DataScope参数实现自动数据权限过滤
7. 操作日志：查看系统操作日志，对每一个接口进行收集
8. 定时任务管理：创建定时任务，维护定时任务的开启和关闭

#### 技术说明
1. spring-cloud+Sping-boot+spring-security+Spring+Sprng-mvc+MyBatis-Plus做的基础功能
2. nacos作为注册中心
3. 所有模块继承security来实现权限控制
4. 使用jwt生成token实现前后端分离的权限校验
5. 引入druid连接池，swagger
6. spring security作为权限控制，实现多角色权限管理
7. 前端框架Vue+Element-ui+Avue
8. 实现动态路由来生成用户菜单，结合security实现按钮权限以及按钮的显示隐藏
9. 前端表格框架使用auve简化对表格的开发,
10. 对axios进行二次封装，统一管理接口请求，统一错误处理
11. 统一实现定时任务管理-通过继承ScheduledOfTask来实现定时任务


#### 部署准备-以下是我使用的版本
1. mysql5.7
2. nacos-server-2.2.3
3. node16.14.1
4. redis
5. maven
6. JDK8
### 后端目录
``````
├─zwzc_admin    系统管理服务，包含用户，角色，菜单，权限，日志，字典，等功能
├─zwzc_auth     认证服务，提供登录接口，验证码获取接口，继承security实现认证
├─zwzc_basics   基础服务，提供定时任务服务，后续可以扩展其他基础服务
├─zwzc_common    公共依赖，
│  ├─zwzc_abnormal  
│  ├─zwzc_config
│  ├─zwzc_constant
│  ├─zwzc_druid_config
│  ├─zwzc_feign   服务调用，所有的feign都在里面，提供调用
│  ├─zwzc_redis   redis工具类
│  ├─zwzc_security
│  ├─zwzc_swagger
│  ├─zwzc_utils   系统工具类
│  └─zwzc_web_config  
├─zwzc_entity   项目实体，项目相关实体，
└─zwzc_gateway   
``````



#### 部署说明
1. 后端
    1. 准备数据库
    2. 准备redis
    3. 准备maven
    4. 准备JDK
    5. 安装运行nacos
    6. 修改配置
        1. 修改nacos配置需,将server‐addr修改配你自己部署的nacos服务地址，
            需要修改的zwzc_admin、zwzc_auth、zwzc_basics、zwzc_gateway,这三个服务
            
        ``````
           spring:
             application:
               name: admin
             main:
               allow-bean-definition-overriding: true  #解决feign的value重复的问题
             cloud:
               nacos:
                 discovery:
                   server‐addr: 192.168.3.170:8848
        ``````
       2. 修改mysql修改成自己的密码帐号
       ``````  
          datasource:
             url: jdbc:mysql://localhost:3306/ht_cloud?useUnicode=true&characterEncoding=utf-8&allowMultiQueries=true&useSSL=false&serverTimezone=UTC
              username: root
              password: root
              type: com.alibaba.druid.pool.DruidDataSource
       ``````
       3. 修改redis配置
       ``````  
          redis:
            database: 0
            host: 127.0.0.1
            port: 6379
            maxIdle: 10
            connectTimeOut: 1000000
            maxTotal: 5000
            timeout: 5000
       ``````    
    7. 运行项目，启动，zwzc_gateway、zwzc_admin、zwzc_auth、zwzc_basics

     
2. 前端
    1. cd /zwzc_cloud_web进入到目录中，
    2. npm i or cnpm i 运行前端依赖安装命令，cnpm 最佳
    3. 修改后端地址，找到文件  /src/util/config.js  找到system配置修改apiUrl为你的后端接口
    4. 运行前端 npm run serve 运行项目
    
#### 演示图

首页
![info](/img/sy.png)

登录
![info](/img/login.png)

用户管理
![info](/img/user.png)

角色管理
![info](/img/role.png)

菜单管理
![info](/img/menu.png)
 
字典管理
![info](/img/dict.png)

部门管理
![info](/img/dept.png)

定时任务管理
![info](/img/task.png)


