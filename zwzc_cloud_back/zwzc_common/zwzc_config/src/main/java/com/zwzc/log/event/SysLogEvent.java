package com.zwzc.log.event;



import com.zwzc.module.sys.entity.SysOperLog;
import org.springframework.context.ApplicationEvent;

/**
 * @Author hejialun
 * @Description: TODO(系统日志事件)
 * @Date: 2022/4/26 15：58：02
 */
public class SysLogEvent extends ApplicationEvent {
	public SysLogEvent(SysOperLog sysOperLog) {
		super(sysOperLog);
	}
}
