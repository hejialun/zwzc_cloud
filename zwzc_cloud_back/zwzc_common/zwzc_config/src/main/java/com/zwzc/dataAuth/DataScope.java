package com.zwzc.dataAuth;

import com.zwzc.module.common.Pager;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>
 * 数据权限对象
 * </p>
 *
 * @author hejialun
 * @since 2022-04-21
 */
@Data
@Accessors(chain = true)
public class DataScope{

	@ApiModelProperty("限制范围的字段名称")
	private String scopeName = "dept_id";

	@ApiModelProperty("自定义数据权限限制范围")
	private List<Integer> deptIds = new ArrayList<>();

	@ApiModelProperty("排序字符串 倒序")
	private String descOrderBy;

	@ApiModelProperty("排序字符串 正序")
	private String ascOrderBy;



	/*
	 * @Author hejialun
	 * @Date 11:16 2022/4/22
	 * @Description TODO(初始化排序值)
	 * @param pager
	 * @return void
	 */
	public DataScope initOrder(Pager pager){
		if(pager.getSortorder().equals("desc")){
			this.setDescOrderBy(pager.getSortdatafield());
		}
		if(pager.getSortorder().equals("asc")){
			this.setAscOrderBy(pager.getSortdatafield());
		}
		return this;
	}

}
