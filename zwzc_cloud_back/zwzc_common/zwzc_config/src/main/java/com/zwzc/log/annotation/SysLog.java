package com.zwzc.log.annotation;

import com.zwzc.DicConstants;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author hejialun
 * @Description: TODO(日志注解)
 * @Date 2021/4/23 10:26
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface SysLog {
	/**
	 * 模块标题-不设置则获取@RequestMapping的值
	 */
	String title() default "";

	/**
	 * 操作说明-必填
	 */
	String msg();

	/**
	 * 操作类型-默认 QUERY
	 */
	String type() default DicConstants.OperLogType.QUERY;
}
