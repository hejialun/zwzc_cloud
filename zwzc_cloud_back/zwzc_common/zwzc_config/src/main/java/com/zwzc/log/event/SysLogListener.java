package com.zwzc.log.event;
import com.zwzc.module.sys.entity.SysOperLog;
import com.zwzc.sys.SysOperLogFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;


/**
 * @Author hejialun
 * @Description: TODO(异步监听日志事件)
 * @Date: 2022/4/26 15：58：02
 */
@Slf4j
@Component
public class SysLogListener {
	@Autowired
	private SysOperLogFeign sysOperLogFeign;

	@Async
	@Order
	@EventListener(SysLogEvent.class)
	public void saveSysLog(SysLogEvent event) throws InterruptedException {
		SysOperLog sysLog = (SysOperLog) event.getSource();
		sysOperLogFeign.add(sysLog);
	}
}
