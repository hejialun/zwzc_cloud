package com.zwzc.sys;


import com.zwzc.module.common.JsonResult;
import com.zwzc.module.sys.vo.SysRoleAuth.UserAuthVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(value = "admin",contextId = "sys-role-auth")
public interface SysRoleAuthFeign {
    /**
     * 通过id获取用户自定义权限
     * @param id：用户id
     * @return
     */
    @GetMapping("/sys-role-auth/findByUserId/{id}")
    JsonResult<UserAuthVo> findByUserId(@PathVariable("id") String id);


}
