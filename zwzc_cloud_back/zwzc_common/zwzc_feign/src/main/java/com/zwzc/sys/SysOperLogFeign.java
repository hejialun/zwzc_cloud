package com.zwzc.sys;


import com.zwzc.module.common.JsonResult;
import com.zwzc.module.sys.entity.SysOperLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@Component
@FeignClient(value = "admin",contextId  = "sys-oper-log")
public interface SysOperLogFeign {
    /**
     * 通过id获取用户自定义权限
     * @param id：用户id
     * @return
     */
    @PostMapping("/sys-oper-log/add")
    JsonResult add(@RequestBody @Valid SysOperLog log);


}
