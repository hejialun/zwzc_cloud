package com.zwzc.sys;

import com.zwzc.module.common.JsonResult;
import com.zwzc.module.sys.entity.SysUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(value = "admin",contextId = "sys-user")
public interface SysUserFeign {

    @GetMapping("/sys-user/getUserInfoByUsernameAll/{username}")
    JsonResult<SysUser> getUserInfoByUsernameAll(@PathVariable("username") String username);

    @GetMapping("/sys-user/getUserInfoByUsername/{username}")
    JsonResult<SysUser> getUserInfoByUsername(@PathVariable("username") String username);
}
