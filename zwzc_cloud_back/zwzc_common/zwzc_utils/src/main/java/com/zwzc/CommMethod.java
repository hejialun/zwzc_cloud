/**
 * 
 */
package com.zwzc;

import cn.hutool.core.util.StrUtil;
import com.zwzc.util.UserUtil;
import lombok.experimental.UtilityClass;
import org.springframework.util.Assert;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

/**
 *
 * @ProjectName: back-end
 * @ClassName: CommMethod
 * @Author: hejialun
 * @Description: 公共静态方法类
 * @Date: 2021/4/22 15:30
 */
@UtilityClass
public class CommMethod {
	private final String UNKNOWN = "unknown";


	public String getIP(HttpServletRequest request) {
		Assert.notNull(request, "HttpServletRequest is null");
		String ip = request.getHeader("X-Requested-For");
		if (StrUtil.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
			ip = request.getHeader("X-Forwarded-For");
		}
		if (StrUtil.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (StrUtil.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (StrUtil.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}
		if (StrUtil.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (StrUtil.isBlank(ip) || UNKNOWN.equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return StrUtil.isBlank(ip) ? null : ip.split(",")[0];
	}


	/**
	 * 创建对象初始化
	 * @param obj
	 */
	public static void beanCreate(Object obj){
		//设置创建时间
		setFieldValueByName(obj,"createDate",LocalDateTime.now());
		//设置创建人
		setFieldValueByName(obj,"createUser", UserUtil.getUserId());
		//设置状态
		setFieldValueByName(obj,"state", DicConstants.State.ENABLE);
		//设置软删除
		setFieldValueByName(obj,"delFlag", BusConstant.DEL_FLAG_N);
	}

	/**
	 * 修改对象初始化
	 * @param obj
	 */
	public static void beanUpdate(Object obj){
		//设置创建时间
		setFieldValueByName(obj,"updateDate",LocalDateTime.now());
		//设置创建人
		setFieldValueByName(obj,"updateUser", UserUtil.getUserId());
	}


	/**
	 * 通过反射写一个类，类中有个方法。
	 * 该方法可以设置某个类中的某个属性（构造方法，成员变量，成员方法）为特定的值
	 * @param obj
	 * @param fieldName
	 * @param value
	 */
	public static void setFieldValueByName(Object obj, String fieldName, Object value){
		try {
			// 获取obj类的字节文件对象
			Class c = obj.getClass();
			// 获取该类的成员变量
			Field f = c.getDeclaredField(fieldName);
			if(f!=null){
				// 取消语言访问检查
				f.setAccessible(true);
				// 给变量赋值
				f.set(obj, value);
			}
		} catch (Exception e) {

		}

	}

	/**
	 * 根据属性名获取属性值
	 * */
	private Object getFieldValueByName(String fieldName, Object o) {
		try {
			String firstLetter = fieldName.substring(0, 1).toUpperCase();
			String getter = "get" + firstLetter + fieldName.substring(1);
			Method method = o.getClass().getMethod(getter, new Class[] {});
			Object value = method.invoke(o, new Object[] {});
			return value;
		} catch (Exception e) {

			return null;
		}
	}

}
