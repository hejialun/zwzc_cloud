package com.zwzc;

import cn.hutool.core.util.StrUtil;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ProjectName: ht
 * @ClassName: MyRedissonConfig
 * @Author: hejialun
 * @Description:
 * @Date: 2021/12/15 15:19
 */
@Configuration
public class RedissonConfig {
    @Value("${spring.redis.host}")
    private String host;
    @Value("${spring.redis.port}")
    private String port;
    @Value("${spring.redis.password:}")
    private String password;

    @Bean("redissonClient")
    RedissonClient redissonClient(){
        //1、创建配置
        Config config = new Config();
        config.useSingleServer()
                .setAddress("redis://"+host+":"+port).setPassword(StrUtil.isEmpty(password)?null:password);
        RedissonClient redissonClient = Redisson.create(config);
        return redissonClient;
    }




}