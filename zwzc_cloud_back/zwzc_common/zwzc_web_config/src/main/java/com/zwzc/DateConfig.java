package com.zwzc;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @ClassName: DateConfig
 * @Author: hejialun
 * @Description: 统一时间格式处理
 * @Date: 2021/4/22 15:18
 */
@Configuration
public class DateConfig {
    private final String LocalDateTimeFormat = "yyyy-MM-dd HH:mm:ss";
    private final String LocalDateFormat = "yyyy-MM-dd";
    private final String LocalTimeFormat = "HH:mm:ss";

    /**
     * @Author hejialun
     * @Description: TODO(设置时间类型接口统一返回格式)
     * @Return
     * @Date 2021/5/1 14:56
     */
    @Bean
    public ObjectMapper serializingObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        //禁止将Date序列化为时间戳
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
//        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.registerModule(new Jdk8Module());
        JavaTimeModule module = new JavaTimeModule();
        // 时间格式
        module.addSerializer(LocalDateTime.class, new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(LocalDateTimeFormat)));
        module.addDeserializer(LocalDateTime.class, new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern(LocalDateTimeFormat)));
        module.addSerializer(LocalDate.class, new LocalDateSerializer(DateTimeFormatter.ofPattern(LocalDateFormat)));
        module.addDeserializer(LocalDate.class, new LocalDateDeserializer(DateTimeFormatter.ofPattern(LocalDateFormat)));
        objectMapper.registerModule(module);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper;
    }

    /**
     * @param
     * @Author hejialun
     * @Description: TODO(日期转换器, java.lang.String - > java.util.Date)
     * @Return org.springframework.core.convert.converter.Converter<java.lang.String, java.util.Date>
     * @Date 2021/4/22 15:19
     */
    @Bean
    public Converter<String, Date> dateConverter() {
        return new Converter<String, Date>() {
            @Override
            public Date convert(String source) {
                SimpleDateFormat df = new SimpleDateFormat(LocalDateTimeFormat);
                Date date = null;
                try {
                    date = df.parse(source);
                } catch (Exception e) {
                    return null;
                }
                return date;
            }
        };
    }


    /**
     * @param
     * @Author hejialun
     * @Description: TODO(日期转换器, java.lang.String - > java.time.LocalDateTime)
     * @Return org.springframework.core.convert.converter.Converter<java.lang.String, java.time.LocalDateTime>
     * @Date 2021/4/22 15:20
     */
    @Bean
    public Converter<String, LocalDateTime> localDateTimeConverter() {
        return new Converter<String, LocalDateTime>() {

            @Override
            public LocalDateTime convert(String source) {
                DateTimeFormatter df = DateTimeFormatter.ofPattern(LocalDateTimeFormat);
                LocalDateTime date = null;
                try {
                    date = LocalDateTime.parse(source, df);
                } catch (Exception e) {
                    return null;
                }
                return date;
            }
        };
    }

    /**
     * @param
     * @Author hejialun
     * @Description: TODO(日期转换器, java.lang.String - > java.time.LocalDate)
     * @Return org.springframework.core.convert.converter.Converter<java.lang.String, java.time.LocalDate>
     * @Date 2021/4/22 15:20
     */
    @Bean
    public Converter<String, LocalDate> LocalDateConverter() {
        return new Converter<String, LocalDate>() {
            @Override
            public LocalDate convert(String source) {
                DateTimeFormatter df = DateTimeFormatter.ofPattern(LocalDateFormat);
                LocalDate date = null;
                try {
                    date = LocalDate.parse(source, df);
                } catch (Exception e) {
                    return null;
                }
                return date;
            }
        };
    }

    /**
     * @param
     * @Author hejialun
     * @Description: TODO(日期转换器, java.lang.String - > java.time.LocalTime)
     * @Return org.springframework.core.convert.converter.Converter<java.lang.String, java.time.LocalTime>
     * @Date 2021/4/22 15:20
     */
    @Bean
    public Converter<String, LocalTime> LocalTimeConvert() {
        return new Converter<String, LocalTime>() {
            @Override
            public LocalTime convert(String source) {
                DateTimeFormatter df = DateTimeFormatter.ofPattern(LocalTimeFormat);
                LocalTime date = null;
                try {
                    date = LocalTime.parse(source, df);
                } catch (Exception e) {
                    return null;
                }
                return date;
            }
        };
    }

}