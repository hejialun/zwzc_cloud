package com.zwzc;

/**
 * 业务异常处理
 */
public class HtException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public HtException(String message) {
        super(message);
    }

    public HtException(Throwable cause) {
        super(cause);
    }

    public HtException(String message, Throwable cause) {
        super(message, cause);
    }
}