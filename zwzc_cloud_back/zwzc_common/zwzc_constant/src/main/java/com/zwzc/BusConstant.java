package com.zwzc;

/**
 * @ProjectName: zwzc
 * @ClassName: BusConstant
 * @Author: hejialun
 * @Description: 业务常量
 * @Date: 2021/6/19 1:06
 */
public class BusConstant {

    /** Token来源 **/
    public final static String X_ACCESS_TOKEN = "X-Access-Token";
    public final static String TOKEN = "token";




    /**
     * 删除标志
     */
    public static final Integer DEL_FLAG_Y = 1;

    /**
     * 未删除
     */
    public static final Integer DEL_FLAG_N = 0;


    /**
     * 验证码过期时间
     */
    public static final Integer VERIFICATION_CODE_OVERDUE_TIME=60;

    /**
     * 验证码不存在
     */
    public static final String VERIFICATION_CODE_NOT_EXIST="请输入验证码!";

    /**
     * 验证码过期
     */
    public static final String VERIFICATION_CODE_PAST="验证码过期!";

    /**
     * 验证码过期
     */
    public static final String VERIFICATION_CODE_NOT_CORRECT="验证码不正确!";


    /**
     * 数据锁定描述
     */
    public static final String LOOK_MESSAGE="当前数据有人正在操作，请稍后!";

    /**
     * 定义日志请求携带参数，标记一次请求的唯一性
     */
    public static final String LOG_KEY="zwzc_log_key";
}