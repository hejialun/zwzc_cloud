package com.zwzc;

/**
 * @ProjectName: zwzc
 * @ClassName: DicConstants
 * @Author: hejialun
 * @Description: 码表常量
 * @Date: 2021/9/14 11:02
 */
public class DicConstants {

    /**
     * 状态
     */
    public static class TaskState {
        private TaskState() {
            throw new UnsupportedOperationException();
        }
        //激活
        public static final String ACTIVE = "01";
        //停止
        public static final String STOP = "02";
    }


    /**
     * 数据权限标识
     */
    public static class DataAuthority {
        private DataAuthority() {
            throw new UnsupportedOperationException();
        }
        //全部数据权限
        public static final String ALL = "01";
        //本部门数据权限
        public static final String DEPT = "02";
        //本部门及下级部门数据权限
        public static final String DEPT_LOWER = "03";
        //本部门及所有子集数据权限
        public static final String DEPT_SUBSET= "04";
        //仅本人数据权限
        public static final String ONESELF = "05";
        //自定义数据权限
        public static final String CUSTOM = "99";
    }


    /**
     * 状态
     */
    public static class State {
        private State() {
            throw new UnsupportedOperationException();
        }
        //启用
        public static final String ENABLE = "01";
        //禁用
        public static final String DISABLE = "02";
    }


    /**
     * 菜单类型
     */
    public static class MenuType {
        private MenuType() {
            throw new UnsupportedOperationException();
        }
        //菜单
        public static final String MENU = "0";
        //页面
        public static final String PAGE = "1";
        //按钮
        public static final String BUTTON = "2";
    }
    /**
     * 操作日志状态
     */
    public static class OperLogStates {
        private OperLogStates() {
            throw new UnsupportedOperationException();
        }
        //正常
        public static final String NORMAL = "0";
        //异常
        public static final String ERROR = "1";
    }

    /**
     * 操作日志类型
     */
    public static class OperLogType {
        private OperLogType() {
            throw new UnsupportedOperationException();
        }
        //查询
        public static final String QUERY = "01";
        //新增
        public static final String ADD = "02";
        //修改
        public static final String UPDATE = "03";
        //删除
        public static final String DEL = "04";
        //登录
        public static final String LOGIN = "05";
        //登出
        public static final String LOGOUT = "06";
        //导入
        public static final String IMPORT = "07";
        //导出
        public static final String EXPORT = "08";
        //激活
        public static final String ACTIVE = "09";
        //停止
        public static final String STOP = "10";
    }





}