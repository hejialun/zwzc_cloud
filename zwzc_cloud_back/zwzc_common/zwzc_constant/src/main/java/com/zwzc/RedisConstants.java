package com.zwzc;

/**
 * @ProjectName: zwzc
 * @ClassName: RedisConstants
 * @Author: hejialun
 * @Description: redis用的常量
 * @Date: 2021/9/17 15:43
 */
public class RedisConstants {
    /**
     * 当前登录用户redis key前缀
     */
    public static final String USER_INFOS_PREFIX="user_infos:";

    /**
     * 验证码前缀
     */
    public static final String VERIFICATION_CODE_PREFIX="verificationCode:";

    /**
     * 码表缓存列表
     */
    public static final String DICT_LIST="dictList";

    /**
     * 用户权限缓存前缀
     */
    public static final String USER_ROLE="USER_ROLE:";

    /**
     * 角色权限key值
     */
    public static final String SYS_ROLE_CODES="sysRoleCodes";

    /**
     * 菜单权限key值
     */
    public static final String SYS_MENU_CODES="sysMenuCodes";

    /**
     * 用户数据权限缓存
     */
    public static final String DATA_AUTHORITY="data_authority:";


    /**
     * 定时任务锁前缀
     */
    public static final String TASK_LOCK_PREFIX="task_lock_prefix:";
}