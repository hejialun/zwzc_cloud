package com.zwzc.handler;

import com.alibaba.fastjson.JSON;
import com.zwzc.enums.ResultEnum;
import com.zwzc.module.common.JsonResult;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ProjectName: zwzc
 * @ClassName: LocalAuthenticationEntryPointHandler
 * @Author: hejialun
 * @Description: 用户未登陆时返回的
 * @Date: 2021/6/21 9:53
 */
@Component
public class LocalAuthenticationEntryPointHandler implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e) throws IOException, ServletException {
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json;charset=utf-8");
        response.getWriter().write(JSON.toJSONString(JsonResult.error(ResultEnum.USER_NEED_AUTHORITIES.getMessage(), ResultEnum.USER_NEED_AUTHORITIES.getCode())));
    }
}