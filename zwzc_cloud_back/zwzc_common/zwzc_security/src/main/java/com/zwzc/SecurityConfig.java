package com.zwzc;

import com.zwzc.filter.JwtAuthenticationTokenFilter;
import com.zwzc.handler.*;
import com.zwzc.util.NoLoginUrl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * @ProjectName: zwzc
 * @ClassName: SecurityConfig
 * @Author: hejialun
 * @Description: security配置类
 * @Date: 2021/6/22 11:56
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    LocalAuthenticationEntryPointHandler authenticationEntryPoint;//未登陆时返回 JSON 格式的数据给前端（否则为 html）
    @Autowired
    private MyUserDetailsService myUserDetailsService;
    @Autowired
    private LocalAuthenticationSuccessHandler localAuthenticationSuccessHandler;    //登录成功处理
    @Autowired
    private LocalAuthenticationFailureHandler localAuthenticationFailureHandler;    //登录失败处理
    @Autowired
    private LocalAccessDeniedHandler localAccessDeniedHandler;    //无权访问处理
    @Autowired
    private LocalLogoutSuccessHandler localLogoutSuccessHandler;    //退出登录

    @Autowired
    private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;  //jwt过滤器

    /**
     * 注入加密对象
     *
     * @return
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    /**
     * 完成security设置
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // 去掉 CSRF
        http.cors().and().csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) // 使用 JWT，关闭token
                .and()
                .httpBasic().authenticationEntryPoint(authenticationEntryPoint)
                .and()
                .formLogin()  //开启登录, 定义当需要用户登录时候，转到的登录页面
//                .loginPage("/test/login.html")
//                .loginProcessingUrl("/login")
                .successHandler(localAuthenticationSuccessHandler) // 登录成功
                .failureHandler(localAuthenticationFailureHandler) // 登录失败
                .permitAll()
                .and()
                .authorizeRequests()//定义哪些URL需要被保护、哪些不需要被保护
                .antMatchers(NoLoginUrl.getArrUrl()).permitAll()       //定义不需要认证的路径//允许匿名注册
                .anyRequest().authenticated()//任何请求,登录后可以访问
                .and()
                .logout()//默认注销行为为logout
                .logoutUrl("/logout")
                .logoutSuccessHandler(localLogoutSuccessHandler)
                .permitAll();

        http.exceptionHandling().accessDeniedHandler(localAccessDeniedHandler); // 无权访问 JSON 格式的数据
        http.addFilterBefore(jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class); // JWT Filter
    }
}