package com.zwzc.util;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.util.AntPathMatcher;

import java.util.Arrays;
import java.util.List;

/**
 * @ClassName NoLoginUrl
 * @Description TODO(定义不需要登录的url)
 * @Author hejialun
 * @Date 2022/2/18 23:54
 * @Version 1.0
 */
public class NoLoginUrl {
    public final static List<String> list = Arrays
            .asList(
                    "/css/**",
                    "/component/**",
                    "/lib/**",
                    "/core/**",
                    "/img/**",
                    "/login.html",
                    "/login",
                    "/sys-login/**",
                    /*放开swagger*/
                    "swagger/**",
                    "/swagger-ui.html",
                    "swagger-ui.html",
                    "/webjars/**",
                    "/swagger-ui.html/*",
                    "/swagger-resources",
                    "/swagger-resources/**",
                    "/csrf",
                    "/v2/**",
                    "/socketServer/**",
                    /*权限认证*/
                    "/sys-user/getUserInfoByUsernameAll/**",
                    "/sys-user/getUserInfoByUsername/**"
            );


    /**
     * @param
     * @return java.lang.String[]
     * @Author hejialun
     * @Date 0:47 2022/2/19
     * @Description TODO(获取数组格式的url)
     */
    public static String[] getArrUrl() {
        return list.toArray(new String[list.size()]);
    }

    /**
     * 判断指定url地址是否匹配指定url集合中的任意一个
     *
     * @param urlPath 指定url地址
     * @return 是否匹配  匹配返回true，不匹配返回false
     */
    public static boolean matches(String urlPath) {
        if (StrUtil.isEmpty(urlPath) || ObjectUtil.isEmpty(list)) {
            return false;
        }
        for (String url : list) {
            if (isMatch(url, urlPath)) {
                return true;
            }
        }
        return false;
    }


    /**
     * 判断url是否与规则配置:
     * ? 表示单个字符
     * * 表示一层路径内的任意字符串，不可跨层级
     * ** 表示任意层路径
     *
     * @param url     匹配规则
     * @param urlPath 需要匹配的url
     * @return
     */
    public static boolean isMatch(String url, String urlPath) {
        AntPathMatcher matcher = new AntPathMatcher();
        return matcher.match(url, urlPath);
    }

}
