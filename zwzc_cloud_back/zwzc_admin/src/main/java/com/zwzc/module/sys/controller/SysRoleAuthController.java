package com.zwzc.module.sys.controller;


import com.zwzc.log.annotation.SysLog;
import com.zwzc.module.common.JsonResult;
import com.zwzc.module.sys.service.ISysRoleAuthService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;


/**
 * <p>
 * 角色自定义权限表 前端控制器
 * </p>
 *
 * @author hejialun
 * @since 2022-04-20
 */
@RestController
@RequestMapping("/sys-role-auth")
public class SysRoleAuthController {

    
    @Autowired
    private ISysRoleAuthService iSysRoleAuthService;




    @GetMapping("/findCustomByRoleId/{roleId}")
    @ApiOperation(value = "通过角色ID获取角色自定义权限")
    @SysLog(msg = "通过角色ID获取角色自定义权限")
    public JsonResult findCustomByRoleId(@PathVariable String roleId){
        return JsonResult.success(iSysRoleAuthService.findCustomByRoleId(roleId));
    }


    @GetMapping("/findByUserId/{id}")
    @ApiOperation(value = "通过用户id获取用户自定义权限")
    @SysLog(msg = "通过用户id获取用户自定义权限")
    public JsonResult findByUserId(@PathVariable String id){
        return JsonResult.success(iSysRoleAuthService.findByUserId(id));
    }
}
