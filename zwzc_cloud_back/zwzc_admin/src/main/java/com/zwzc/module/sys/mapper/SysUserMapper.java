package com.zwzc.module.sys.mapper;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.zwzc.dataAuth.DataScope;
import com.zwzc.module.common.Pager;
import com.zwzc.module.sys.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Auto-generator
 * @since 2021-04-27
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

    Pager<SysUser> findPage(@Param("pager") Pager<SysUser> pager, @Param(Constants.WRAPPER) QueryWrapper<SysUser> qw);

    Pager<SysUser> findPageAuth(@Param("pager") Pager<SysUser> pager, @Param(Constants.WRAPPER) QueryWrapper<SysUser> qw, DataScope dataScope);
}
