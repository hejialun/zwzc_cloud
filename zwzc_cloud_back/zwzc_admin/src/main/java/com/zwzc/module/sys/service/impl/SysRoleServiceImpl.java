package com.zwzc.module.sys.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zwzc.CommMethod;
import com.zwzc.DicConstants;
import com.zwzc.module.sys.entity.SysMenuRole;
import com.zwzc.module.sys.entity.SysRole;
import com.zwzc.module.sys.entity.SysRoleAuth;
import com.zwzc.module.sys.mapper.SysRoleMapper;
import com.zwzc.module.sys.service.ISysMenuRoleService;
import com.zwzc.module.sys.service.ISysRoleAuthService;
import com.zwzc.module.sys.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zwzc.module.common.Pager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Auto-generator
 * @since 2021-04-27
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {
    @Autowired
    private ISysMenuRoleService iMenuRoleService;
    @Resource
    private ISysRoleAuthService sysRoleAuthService;

    @Override
    public Pager<SysRole> findPage(Pager<SysRole> pager, SysRole en) {
        QueryWrapper<SysRole> qw=new QueryWrapper<>();
        if(StrUtil.isNotEmpty(en.getCode())){
            qw.like("code",en.getCode());
        }
        if(StrUtil.isNotEmpty(en.getName())){
            qw.like("name",en.getName());
        }

        return baseMapper.selectPage(pager,qw);
    }

    @Override
    public List<SysRole> findByUserId(String userId) {
        return baseMapper.findByUserId(userId);
    }

    @Override
    @Transactional
    public void setMenuByRole(SysRole sysRole) {
        //删除原来的所有菜单
        iMenuRoleService.remove(new QueryWrapper<SysMenuRole>().eq("role_id", sysRole.getId()));
        List<SysMenuRole> menuRoles=new ArrayList<>();;
        //录入新的菜单
        sysRole.getMenus().forEach(x -> {
            SysMenuRole menuRole = new SysMenuRole();
            menuRole.setRoleId(sysRole.getId());
            menuRole.setMenuId(x.getId());
            menuRoles.add(menuRole);
        });
        iMenuRoleService.saveBatch(menuRoles);
    }

    @Override
    public List<SysRole> findList(SysRole en) {
        QueryWrapper<SysRole> qw=new QueryWrapper<>();
        if(StrUtil.isNotEmpty(en.getCode())){
            qw.like("code",en.getCode());
        }
        if(StrUtil.isNotEmpty(en.getName())){
            qw.like("name",en.getName());
        }
        return baseMapper.selectList(qw);
    }

    @Override
    @Transactional
    public void add(SysRole sysRole) {
        //插入角色
        CommMethod.beanCreate(sysRole);
        baseMapper.insert(sysRole);
        if(sysRole.getDataAuthority().equals(DicConstants.DataAuthority.CUSTOM)){
            //插入自定义角色
            List<SysRoleAuth> auths = sysRole.getCustom().stream().map(x -> {
                SysRoleAuth auth = new SysRoleAuth();
                auth.setDeptId(x);
                auth.setRoleId(sysRole.getId());
                return auth;
            }).collect(Collectors.toList());
            sysRoleAuthService.saveBatch(auths);
        }
    }

    @Override
    @Transactional
    public void editById(SysRole sysRole) {
        CommMethod.beanUpdate(sysRole);
        baseMapper.updateById(sysRole);

        //删除旧的自定义权限
        sysRoleAuthService.remove(
                new QueryWrapper<SysRoleAuth>()
                    .eq("role_id",sysRole.getId())
        );

        if(sysRole.getDataAuthority().equals(DicConstants.DataAuthority.CUSTOM)){
            //插入自定义角色
            List<SysRoleAuth> auths = sysRole.getCustom().stream().map(x -> {
                SysRoleAuth auth = new SysRoleAuth();
                auth.setDeptId(x);
                auth.setRoleId(sysRole.getId());
                return auth;
            }).collect(Collectors.toList());
            sysRoleAuthService.saveBatch(auths);
        }


    }


}
