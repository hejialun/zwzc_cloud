package com.zwzc.module.sys.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zwzc.module.common.Pager;
import com.zwzc.module.sys.entity.SysOperLog;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

/**
 * <p>
 * 系统操作日志 Mapper 接口
 * </p>
 *
 * @author hejialun
 * @since 2022-04-26
 */
public interface SysOperLogMapper extends BaseMapper<SysOperLog> {

    Pager<SysOperLog> findPage(Pager<SysOperLog> pager, @Param(Constants.WRAPPER) QueryWrapper<SysOperLog> qw);
}
