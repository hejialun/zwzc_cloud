package com.zwzc.module.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zwzc.module.sys.entity.SysMenu;

import java.util.List;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-13
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    List<SysMenu> findByUserId(String id);
}
