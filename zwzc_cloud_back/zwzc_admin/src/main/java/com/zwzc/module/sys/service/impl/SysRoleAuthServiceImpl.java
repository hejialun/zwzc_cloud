package com.zwzc.module.sys.service.impl;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zwzc.DicConstants;
import com.zwzc.RedisConstants;
import com.zwzc.RedisUtil;
import com.zwzc.module.sys.entity.SysDept;
import com.zwzc.module.sys.entity.SysRole;
import com.zwzc.module.sys.entity.SysRoleAuth;
import com.zwzc.module.sys.entity.SysUser;
import com.zwzc.module.sys.mapper.SysRoleAuthMapper;
import com.zwzc.module.sys.service.ISysDeptService;
import com.zwzc.module.sys.service.ISysRoleAuthService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zwzc.module.sys.service.ISysUserRoleService;
import com.zwzc.module.sys.service.ISysUserService;
import com.zwzc.module.sys.vo.SysRoleAuth.UserAuthVo;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 角色自定义权限表 服务实现类
 * </p>
 *
 * @author hejialun
 * @since 2022-04-20
 */
@Service
public class SysRoleAuthServiceImpl extends ServiceImpl<SysRoleAuthMapper, SysRoleAuth> implements ISysRoleAuthService {
    @Resource
    private ISysUserRoleService sysUserRoleService;
    @Resource
    private ISysDeptService sysDeptService;
    @Resource
    private ISysUserService sysUserService;
    @Resource
    private RedisUtil redisUtil;

    @Override
    public List<SysRoleAuth> findCustomByRoleId(String roleId) {
        return baseMapper.selectList(
                new QueryWrapper<SysRoleAuth>()
                    .eq("role_id",roleId)
        );
    }

    @Override
    public UserAuthVo findByUserId(String id) {

        //获取数据权限
        UserAuthVo vo = JSON.parseObject(redisUtil.get(RedisConstants.DATA_AUTHORITY + id)+"",UserAuthVo.class);
        if(ObjectUtil.isNotEmpty(vo)){
            return vo;
        }

        //获取当前用户
        SysUser sysUser=sysUserService.getById(id);
        //获取当前用户的角色
        List<SysRole> sysRoles = sysUserRoleService.findRoleByUserId(id);

        //定义数据权限集合
        Set<String> deptIds=new LinkedHashSet<>();
        //定义用户数据全新啊
        Set<String> userIds=new LinkedHashSet<>();

        //遍历角色-求当前用户的并集权限
        for (SysRole role : sysRoles) {
            switch (role.getDataAuthority()){
                case DicConstants.DataAuthority.ALL:             //全部数据权限
                    //查询出所有部门
                    deptIds.addAll(sysDeptService.list().stream().map(SysDept::getDeptId).collect(Collectors.toSet()));
                    break;
                case DicConstants.DataAuthority.DEPT:            //本部门数据权限
                    deptIds.addAll(
                            sysDeptService.list(
                                    new QueryWrapper<SysDept>()
                                            .eq("dept_id",sysUser.getDeptId())
                            ).stream().map(SysDept::getDeptId).collect(Collectors.toSet())
                    );
                    break;
                case DicConstants.DataAuthority.DEPT_LOWER:      //本部门及下级部门数据权限
                    deptIds.add(sysUser.getDeptId());
                    deptIds.addAll(
                            sysDeptService.list(
                                    new QueryWrapper<SysDept>()
                                            .eq("parent_id",sysUser.getDeptId())
                            ).stream().map(SysDept::getDeptId).collect(Collectors.toSet())
                    );
                    break;
                case DicConstants.DataAuthority.DEPT_SUBSET:      //本部门及所有子集数据权限
                    deptIds.add(sysUser.getDeptId());
                    deptIds.addAll(sysDeptService.findSubsetById(sysUser.getDeptId()).stream().map(Tree::getId).collect(Collectors.toSet()));
                    break;
                case DicConstants.DataAuthority.ONESELF:         //仅本人数据权限
                    userIds.add(sysUser.getId());
                    break;
                case DicConstants.DataAuthority.CUSTOM:          //自定义数据权限
                    //查询自定义数据权限
                    deptIds.addAll(findCustomByRoleId(role.getId()).stream().map(SysRoleAuth::getDeptId).collect(Collectors.toSet()));
                    break;
            }
        }
        //定义数据权限对象
        vo=new UserAuthVo().setDeptIds(deptIds).setUser(userIds);

        //缓存
        redisUtil.set(RedisConstants.DATA_AUTHORITY+id, JSON.toJSONString(vo));
        return vo;
    }




}
