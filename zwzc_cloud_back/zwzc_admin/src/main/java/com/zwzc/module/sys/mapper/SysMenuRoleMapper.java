package com.zwzc.module.sys.mapper;

import com.zwzc.module.sys.entity.SysMenuRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 权限菜单表 Mapper 接口
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-13
 */
public interface SysMenuRoleMapper extends BaseMapper<SysMenuRole> {

}
