package com.zwzc.module.sys.service;

import com.zwzc.module.sys.dto.UpdatePasswordDto;
import com.zwzc.module.sys.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zwzc.module.sys.vo.SysUser.UserLoginVo;
import com.zwzc.module.common.Pager;

import java.security.Principal;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Auto-generator
 * @since 2021-04-27
 */
public interface ISysUserService extends IService<SysUser> {
    /*
     * @param username:用户名
     * @Author hejialun
     * @Description: TODO(通过用户名获取用户)
     * @date 2021/5/14 20:58
     * @returns com.zwzc.module.sys.entity.SysUser
     */
    SysUser getUserInfoByUsername(String username);

    /*
     * @param username:用户名
     * @Author hejialun
     * @Description: TODO(通过用户名获取用户权限等信息)
     * @date 2021/5/14 20:58
     * @returns com.zwzc.module.sys.entity.SysUser
     */
    SysUser getUserInfoByUsernameAll(String username);

    /*
     * @param id:用户id
     * @Author hejialun
     * @Description: TODO(通过id获取用户信息)
     * @date 2021/5/14 20:58
     * @returns com.zwzc.module.sys.entity.SysUser
     */
    SysUser getUserByid(String id);


    /*
     * @param principal
     * @Author hejialun
     * @Description: TODO(获取登录用户)
     * @date 2021/5/14 21:04
     * @returns com.zwzc.module.sys.entity.SysUser
     */
    SysUser getLoginUser(Principal principal);


    /*
     * @param pager:分页参数
     * @param en：实体
     * @Author hejialun
     * @Description: TODO(分页查询)
     * @date 2021/5/15 20:12
     * @returns com.zwzc.util.Pager
     */
    Pager findPage(Pager<SysUser> pager, SysUser en);

    /**
     * 保存用户
     * @param sysUser：用户实体
     */
    void saveObj(SysUser sysUser);

    /**
     * 重置密码
     * @param id:用户id
     */
    void resetPassword(String id);

    /**
     * 设置用户角色
     * @param sysUser
     */
    void setRoles(SysUser sysUser);

    /**
     * @Author hejialun
     * @Date 15:33 2022/7/20
     * @Description TODO(修改登录用户密码)
     * @param en
     * @return void
     */
    void updatePassword(UpdatePasswordDto en);

    /*
     * @param id：主键id
     * @param val：修改后的值
     * @Author hejialun
     * @Description: TODO(修改用户状态)
     * @date 2021/9/14 16:33
     * @returns void
     */
    void updateState(String id, String val);


    /**
     * @Author hejialun
     * @Date 17:26 2022/12/7
     * @Description TODO(通过用户名登录某个用户，并返回登录信息以及token)
     * @param userName:用户名
     * @return com.zwzc.module.sys.vo.SysUser.UserLoginVo
     */
    UserLoginVo loginUser(String userName);
}
