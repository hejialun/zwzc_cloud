package com.zwzc.module.sys.service;

import com.zwzc.module.sys.entity.SysRole;
import com.zwzc.module.sys.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Auto-generator
 * @since 2021-04-27
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

    /*
     * @param id:用户id
     * @Author hejialun
     * @Description: TODO(通过用户id查询用户角色)
     * @date 2021/5/12 22:00
     * @return
     */
    List<SysRole> findRoleByUserId(String id);

    /*
     * @param id:用户id
     * @param role：权限编码
     * @Author hejialun
     * @Description: TODO(向指定用户插入指定权限)
     * @date 2021/10/18 17:46
     * @returns void
     */
    void saveSpecifiedRole(String id, String role);
}
