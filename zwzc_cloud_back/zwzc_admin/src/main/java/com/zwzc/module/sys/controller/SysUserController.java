package com.zwzc.module.sys.controller;


import com.zwzc.CommMethod;
import com.zwzc.DicConstants;
import com.zwzc.log.annotation.SysLog;
import com.zwzc.module.common.JsonResult;
import com.zwzc.module.common.Pager;
import com.zwzc.module.sys.dto.UpdatePasswordDto;
import com.zwzc.module.sys.entity.SysUser;
import com.zwzc.module.sys.service.ISysUserService;
import com.zwzc.util.UserUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.security.Principal;


/**
 * <p>
 *  用户表 前端控制器
 * </p>
 *
 * @author Auto-generator
 * @since 2021-04-27
 */
@RestController
@RequestMapping("/sys-user")
@Api(tags = "用户 ")
public class SysUserController {


    @Autowired
    private ISysUserService iSysUserService;



    @ApiOperation(value = "通过用户名获取用户权限等信息")
    @GetMapping("/getUserInfoByUsernameAll/{username}")
    public JsonResult<SysUser> getUserInfoByUsernameAll(@PathVariable String username){
        return JsonResult.success(iSysUserService.getUserInfoByUsernameAll(username));
    }


    @ApiOperation(value = "通过用户名获取用户")
    @GetMapping("/getUserInfoByUsername/{username}")
    public JsonResult getUserInfoByUsername(@PathVariable String username){
        return JsonResult.success(iSysUserService.getUserInfoByUsername(username));
    }



    @ApiOperation(value = "修改密码")
    @PostMapping("/updatePassword")
    public JsonResult updatePassword(@RequestBody @Valid UpdatePasswordDto en){
        iSysUserService.updatePassword(en);
        return JsonResult.success();
    }

    @ApiOperation(value = "修改用户状态")
    @PostMapping("/updateState/{id}/{val}")
    @PreAuthorize("hasAnyAuthority('SYS_USER_UPDATE_STATE')")
    @SysLog(msg = "修改用户状态",type = DicConstants.OperLogType.UPDATE)
    public JsonResult updateState(@PathVariable String id, @PathVariable String val){
        iSysUserService.updateState(id,val);
        return JsonResult.success();
    }

    @ApiOperation(value = "设置用户角色")
    @PostMapping("/setRoles")
    @PreAuthorize("hasAnyAuthority('SYS_USER_SET_ROLE')")
    @SysLog(msg = "设置用户角色",type = DicConstants.OperLogType.UPDATE)
    public JsonResult setRoles(@RequestBody SysUser sysUser){
        iSysUserService.setRoles(sysUser);
        return JsonResult.success();
    }

    @ApiOperation(value = "重置密码")
    @PostMapping("/resetPassword/{id}")
    @PreAuthorize("hasAnyAuthority('SYS_USER_RESET_PASSWORD')")
    @SysLog(msg = "重置用户密码",type = DicConstants.OperLogType.UPDATE)
    public JsonResult resetPassword(@PathVariable(value = "id") String id){
        iSysUserService.resetPassword(id);
        return JsonResult.success();
    }

    @ApiOperation(value = "获取登录用户信息")
    @GetMapping("/getLoginUser")
    @SysLog(msg = "获取用户信息")
    public JsonResult getLoginUser(Principal principal){
        return JsonResult.success(iSysUserService.getLoginUser(principal));
    }

    @GetMapping("/findPage")
    @ApiOperation(value = "分页查询")
    @SysLog(msg = "分页查询用户信息")
    public JsonResult findPage(Pager<SysUser> pager, SysUser en){
        return JsonResult.success(iSysUserService.findPage(pager,en));
    }

    @GetMapping("/get-by-id/{id}")
    @ApiOperation(value = "通过id查询")
    @SysLog(msg = "通过用户id查询用户")
    public JsonResult getById(@PathVariable(value = "id") String id){
        System.out.println(UserUtil.getUser());
        return JsonResult.success(iSysUserService.getById(id));
    }

    @PostMapping("/add")
    @ApiOperation(value = "新增")
    @PreAuthorize("hasAnyAuthority('SYS_USER_ADD')")
    @SysLog(msg = "新增用户",type = DicConstants.OperLogType.ADD)
    public JsonResult add(@RequestBody @Validated SysUser sysUser){
        CommMethod.beanCreate(sysUser);
        iSysUserService.saveObj(sysUser);
        return JsonResult.success();
    }

    @DeleteMapping("/delete-by-id/{id}")
    @ApiOperation(value = "删除")
    @PreAuthorize("hasAnyAuthority('SYS_USER_DEL')")
    @SysLog(msg = "删除用户",type = DicConstants.OperLogType.DEL)
    public JsonResult delete(@PathVariable(value = "id") String id){
        iSysUserService.removeById(id);
        return JsonResult.success();
    }

    @PutMapping("/update")
    @ApiOperation(value = "修改")
    @PreAuthorize("hasAnyAuthority('SYS_USER_UPDATE')")
    @SysLog(msg = "修改用户",type = DicConstants.OperLogType.UPDATE)
    public JsonResult updateById(@RequestBody @Validated SysUser sysUser){
        CommMethod.beanUpdate(sysUser);
        iSysUserService.updateById(sysUser);
        return JsonResult.success();
    }

}
