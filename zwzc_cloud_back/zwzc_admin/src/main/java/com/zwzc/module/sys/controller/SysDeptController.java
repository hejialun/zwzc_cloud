package com.zwzc.module.sys.controller;

import com.zwzc.CommMethod;
import com.zwzc.DicConstants;
import com.zwzc.log.annotation.SysLog;
import com.zwzc.module.common.JsonResult;
import com.zwzc.module.sys.entity.SysDept;
import com.zwzc.module.sys.service.ISysDeptService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * <p>
 * 部门表 前端控制器
 * </p>
 *
 * @author hejialun
 * @since 2022-04-18
 */
@Api(tags = "部门管理 ")
@RestController
@RequestMapping("/sys-dept")
public class SysDeptController {

    
    @Autowired
    private ISysDeptService iSysDeptService;

    



    @ApiOperation("获取部门树携带数据权限")
    @GetMapping("/findTreeAuth")
    @SysLog(msg = "携带权限查询部门树")
    public JsonResult findTreeAuth(SysDept en){
        return JsonResult.success(iSysDeptService.findTreeAuth(en));
    }



    @ApiOperation("获取部门树")
    @GetMapping("/findTree")
    @SysLog(msg = "获取部门树")
    public JsonResult findTree(SysDept en){
        return JsonResult.success(iSysDeptService.findTree(en));
    }




    @ApiOperation("新增")
    @PostMapping("/add")
    @PreAuthorize("hasAnyAuthority('SYS_DEPT_ADD')")
    @SysLog(msg = "新增部门",type = DicConstants.OperLogType.ADD)
    public JsonResult add(@RequestBody @Valid SysDept en){
	    CommMethod.beanCreate(en);
        iSysDeptService.add(en);
        return JsonResult.success();
    }


    @ApiOperation("通过id删除")
    @DeleteMapping("/delete-by-id/{id}")
    @PreAuthorize("hasAnyAuthority('SYS_DEPT_DEL')")
    @SysLog(msg = "通过id删除部门",type = DicConstants.OperLogType.DEL)
    public JsonResult delete(@PathVariable(value = "id") String id){
        iSysDeptService.delById(id);
        return JsonResult.success();
    }


    @ApiOperation("修改")
    @PutMapping("/update")
    @PreAuthorize("hasAnyAuthority('SYS_DEPT_UPDATE')")
    @SysLog(msg = "修改部门",type = DicConstants.OperLogType.UPDATE)
    public JsonResult updateById(@RequestBody @Valid SysDept en){
	    CommMethod.beanUpdate(en);
        iSysDeptService.editById(en);
        return JsonResult.success();
    }

}
