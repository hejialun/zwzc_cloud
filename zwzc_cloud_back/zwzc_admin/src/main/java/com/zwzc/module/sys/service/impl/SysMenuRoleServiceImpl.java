package com.zwzc.module.sys.service.impl;

import com.zwzc.module.sys.entity.SysMenuRole;
import com.zwzc.module.sys.mapper.SysMenuRoleMapper;
import com.zwzc.module.sys.service.ISysMenuRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 权限菜单表 服务实现类
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-13
 */
@Service
public class SysMenuRoleServiceImpl extends ServiceImpl<SysMenuRoleMapper, SysMenuRole> implements ISysMenuRoleService {

}
