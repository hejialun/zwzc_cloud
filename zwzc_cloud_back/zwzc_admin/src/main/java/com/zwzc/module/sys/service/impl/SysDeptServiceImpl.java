package com.zwzc.module.sys.service.impl;

import cn.hutool.core.lang.tree.Tree;
import cn.hutool.core.lang.tree.TreeNodeConfig;
import cn.hutool.core.lang.tree.TreeUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zwzc.BusConstant;
import com.zwzc.HtException;
import com.zwzc.dataAuth.DataScope;
import com.zwzc.module.sys.entity.SysDept;
import com.zwzc.module.sys.mapper.SysDeptMapper;
import com.zwzc.module.sys.service.ISysDeptService;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 部门表 服务实现类
 * </p>
 *
 * @author hejialun
 * @since 2022-04-18
 */
@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements ISysDeptService {

    @Override
    public List<Tree<String>> findSubsetById(String id) {
        List<Tree<String>> trees = this.findTree(new SysDept());
        //找到当前节点
        Tree<String> node=null;
        for (Tree<String> tree : trees) {
            node = TreeUtil.getNode(tree, id);
        }
        //定义子集
        List<Tree<String>> subset=new ArrayList<>();

        if(node!=null){
            //找到了节点递归遍历当前节点获取所有子节点
            ergodicNode(node.getChildren(),subset);
        }
        return subset;
    }


    private void ergodicNode(List<Tree<String>> children, List<Tree<String>> subset){
        for (Tree<String> child : children) {
            subset.add(child);
            if(child.getChildren()!=null && child.getChildren().size()>0){
                //递归
                ergodicNode(child.getChildren(),subset);
            }
        }
    }




    @Override
    public List<Tree<String>> findTree(SysDept en) {
        QueryWrapper<SysDept> qw = getQW(en);
        List<SysDept> list = baseMapper.findList(qw);

        return buildTree(list);
    }

    @Override
    public List<Tree<String>> findTreeAuth(SysDept en) {
        QueryWrapper<SysDept> qw = getQW(en);
        List<SysDept> list = baseMapper.findListAuth(qw,new DataScope());
        return buildTree(list);
    }

    private QueryWrapper<SysDept> getQW(SysDept en){
        QueryWrapper<SysDept> qw=new QueryWrapper<>();
        if(StrUtil.isNotEmpty(en.getDeptCode())){
            qw.eq("tab.dept_code",en.getDeptCode());
        }
        if(StrUtil.isNotEmpty(en.getDeptName())){
            qw.eq("tab.dept_name",en.getDeptName());
        }
        qw.eq("tab.del_flag", BusConstant.DEL_FLAG_N);
        qw.orderByAsc("tab.sort");
        return qw;
    }



    @Override
    public void delById(String id) {
        //判断是否有子集
        if(baseMapper.selectCount(
                new QueryWrapper<SysDept>()
                        .eq("parent_id",id)
        )>0){
            throw new HtException("当前部门，还有子集不可删除！");
        }
        baseMapper.deleteById(id);
    }

    @Override
    public void add(SysDept en) {
        if(StrUtil.isEmpty(en.getParentId())){
            en.setParentId("0");
        }
        baseMapper.insert(en);
    }

    @Override
    public void editById(SysDept en) {
        if(StrUtil.isEmpty(en.getParentId())){
            en.setParentId("0");
        }
        baseMapper.updateById(en);
    }



    /**
     * @Author hejialun
     * @Date 16:28 2022/4/18
     * @Description TODO(构建部门树)
     * @param depts
     * @return java.util.List<com.zwzc.module.sys.entity.SysDept>
     */
    private List<Tree<String>> buildTree(List<SysDept> depts){
        //构建树配置
        TreeNodeConfig config=new TreeNodeConfig();
        config.setIdKey("deptId");
        config.setParentIdKey("parentId");
        //定义转换器
        List<Tree<String>> build = TreeUtil.build(depts, "0", config, (node, tree) -> {
            tree.setId(node.getDeptId());
            tree.setParentId(node.getParentId());
            tree.setName(node.getDeptName());
            initExtra(node,tree);
        });
        return build;
    }

    /**
     * @Author hejialun
     * @Date 16:38 2022/4/18
     * @Description TODO(初始化扩展参数-部门具体参数)
     * @param node：节点对象
     * @param tree：树数据
     * @return void
     */
    private void initExtra(Object node,Tree<String> tree){
        //获取clazz
        Class<?> a = node.getClass();
        //获取对象的所有变量
        Field[] fields = a.getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                tree.putExtra(field.getName(),field.get(node));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

    }





}
