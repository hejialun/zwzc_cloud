package com.zwzc.module.sys.controller;



import com.zwzc.DicConstants;
import com.zwzc.log.annotation.SysLog;
import com.zwzc.module.common.JsonResult;
import com.zwzc.module.common.Pager;
import com.zwzc.module.sys.entity.SysRole;
import com.zwzc.module.sys.service.ISysRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import java.time.LocalDateTime;


/**
 * <p>
 *  角色表 前端控制器
 * </p>
 *
 * @author Auto-generator
 * @since 2021-04-27
 */
@RestController
@RequestMapping("/sys-role")
@Api(tags = "角色 ")
public class SysRoleController {

    
    @Autowired
    private ISysRoleService iSysRoleService;






    @PostMapping("/setMenuByRole")
    @ApiOperation(value = "给当前角色设置菜单")
    @PreAuthorize("hasAnyAuthority('SYS_SET_MENU')")
    public JsonResult setMenuByRole(@RequestBody SysRole sysRole) {
        iSysRoleService.setMenuByRole(sysRole);
        return JsonResult.success();
    }


    @GetMapping("/findPage")
    @ApiOperation(value = "分页查询")
    @SysLog(msg = "分页查询角色")
    public JsonResult findPage(Pager<SysRole> pager, SysRole en){
        return JsonResult.success(iSysRoleService.findPage(pager,en));
    }



    @GetMapping("/findList")
    @ApiOperation(value = "查询角色")
    @SysLog(msg = "列表查询角色")
    public JsonResult findList( SysRole en){
        return JsonResult.success(iSysRoleService.findList(en));
    }






    @GetMapping("/get-by-id/{id}")
    @ApiOperation(value = "通过id查询")
    public JsonResult getById(@PathVariable(value = "id") String id){
        return JsonResult.success(iSysRoleService.getById(id));
    }

    @PostMapping("/add")
    @ApiOperation(value = "新增")
    @PreAuthorize("hasAnyAuthority('SYS_ROLE_ADD')")
    @SysLog(msg = "新增角色",type = DicConstants.OperLogType.ADD)
    public JsonResult add(@RequestBody @Validated SysRole sysRole){
        iSysRoleService.add(sysRole);
        return JsonResult.success();
    }


    @DeleteMapping("/delete-by-id/{id}")
    @ApiOperation(value = "删除")
    @PreAuthorize("hasAnyAuthority('SYS_ROLE_DEL')")
    @SysLog(msg = "删除角色",type = DicConstants.OperLogType.DEL)
    public JsonResult delete(@PathVariable(value = "id") String id){
        iSysRoleService.removeById(id);
        return JsonResult.success();
    }


    @PutMapping("/update")
    @ApiOperation(value = "修改")
    @PreAuthorize("hasAnyAuthority('SYS_ROLE_UPDATE')")
    @SysLog(msg = "修改角色",type = DicConstants.OperLogType.UPDATE)
    public JsonResult updateById(@RequestBody @Validated SysRole sysRole){
        iSysRoleService.editById(sysRole);
        return JsonResult.success();
    }

}
