package com.zwzc.module.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;

import com.zwzc.DicConstants;
import com.zwzc.log.annotation.SysLog;
import com.zwzc.module.common.JsonResult;
import com.zwzc.module.common.Pager;
import com.zwzc.module.sys.entity.SysMenu;
import com.zwzc.module.sys.service.ISysMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.util.CollectionUtils;
import java.util.*;


/**
 * <p>
 * 菜单表 前端控制器
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-13
 */
@RestController
@RequestMapping("/sys-menu")
@Api(tags = "菜单 ")
public class SysMenuController {

    
    @Autowired
    private ISysMenuService iSysMenuService;


    @GetMapping("/findMenuRole/{rid}")
    @ApiOperation(value = "按角色查询出菜单")
    @SysLog(msg = "按角色查询出菜单")
    public JsonResult findMenuRole(@PathVariable(value = "rid") String rid){
        return JsonResult.success(iSysMenuService.findMenuRole(rid));
    }



    @GetMapping("/findMenuTree")
    @ApiOperation(value = "查询出菜单树")
    @PreAuthorize("hasAnyAuthority('SYS_MENU')")
    @SysLog(msg = "查询出菜单树")
    public JsonResult findMenuTree(){
        return JsonResult.success(iSysMenuService.findMenuTree());
    }


    @GetMapping("/findPage")
    @ApiOperation(value = "分页查询")
    @PreAuthorize("hasAnyAuthority('SYS_MENU')")
    @SysLog(msg = "分页查询菜单树")
    public JsonResult findPage(Pager<SysMenu> pager, SysMenu sysMenu){
        return JsonResult.success(iSysMenuService.page(pager));
    }




    @PostMapping("/add")
    @ApiOperation(value = "新增")
    @PreAuthorize("hasAnyAuthority('SYS_MENU_ADD')")
    @SysLog(msg = "新增菜单",type = DicConstants.OperLogType.ADD)
    public JsonResult add(@RequestBody  @Validated SysMenu sysMenu){
        iSysMenuService.save(sysMenu);
        return JsonResult.success();
    }

    @DeleteMapping("/delete-by-id/{id}")
    @ApiOperation(value = "删除")
    @PreAuthorize("hasAnyAuthority('SYS_MENU_DEL')")
    @SysLog(msg = "删除菜单",type = DicConstants.OperLogType.DEL)
    public JsonResult delete(@PathVariable(value = "id") String id){
        iSysMenuService.delById(id);
        return JsonResult.success();
    }


    @PutMapping("/update")
    @ApiOperation(value = "修改")
    @PreAuthorize("hasAnyAuthority('SYS_MENU_UPDATE')")
    @SysLog(msg = "修改菜单",type = DicConstants.OperLogType.UPDATE)
    public JsonResult updateById(@RequestBody  @Validated SysMenu sysMenu){
        iSysMenuService.updateById(sysMenu);
        return JsonResult.success();
    }

}
