package com.zwzc.module.sys.service;

import com.zwzc.module.common.Pager;
import com.zwzc.module.sys.entity.SysOperLog;
import com.baomidou.mybatisplus.extension.service.IService;


/**
 * <p>
 * 系统操作日志 服务类
 * </p>
 *
 * @author hejialun
 * @since 2022-04-26
 */
public interface ISysOperLogService extends IService<SysOperLog> {

    Pager<SysOperLog> findPage(Pager<SysOperLog> pager, SysOperLog en);
}
