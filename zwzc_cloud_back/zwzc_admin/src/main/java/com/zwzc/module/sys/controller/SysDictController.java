package com.zwzc.module.sys.controller;


import com.zwzc.CommMethod;
import com.zwzc.DicConstants;
import com.zwzc.log.annotation.SysLog;
import com.zwzc.module.common.JsonResult;
import com.zwzc.module.common.Pager;
import com.zwzc.module.sys.entity.SysDict;
import com.zwzc.module.sys.service.ISysDictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.util.CollectionUtils;


import javax.validation.Valid;
import java.util.*;


/**
 * <p>
 * 字典表 前端控制器
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-19
 */
@Api(tags = "字典表 ")
@RestController
@RequestMapping("/sys-dict")
public class SysDictController {

    
    @Autowired
    private ISysDictService iSysDictService;

    @ApiOperation(value = "分页查询")
    @GetMapping("/findPage")
    @PreAuthorize("hasAnyAuthority('SYS_DICT')")
    @SysLog(msg = "分页查询字典")
    public JsonResult findPage(Pager<SysDict> pager, SysDict sysDict){
        return JsonResult.success(iSysDictService.findPage(pager,sysDict));
    }


    @ApiOperation(value = "通过id查询")
    @GetMapping("/get-by-id/{id}")
    @SysLog(msg = "通过id查询字典")
    public JsonResult getById(@PathVariable(value = "id") String id){
        return JsonResult.success(iSysDictService.getById(id));
    }

    @ApiOperation(value = "新增")
    @PostMapping("/add")
    @PreAuthorize("hasAnyAuthority('SYS_DICT_ADD')")
    @SysLog(msg = "新增字典",type = DicConstants.OperLogType.ADD)
    public JsonResult add(@RequestBody @Validated SysDict sysDict){
        CommMethod.beanCreate(sysDict);
        iSysDictService.save(sysDict);
        return JsonResult.success();
    }


    @ApiOperation(value = "删除")
    @DeleteMapping("/delete-by-id/{id}")
    @PreAuthorize("hasAnyAuthority('SYS_DICT_DEL')")
    @SysLog(msg = "删除字典",type = DicConstants.OperLogType.DEL)
    public JsonResult delete(@PathVariable(value = "id") String id){
        iSysDictService.delById(id);
        return JsonResult.success();
    }

    @ApiOperation(value = "修改字典")
    @PutMapping("/update")
    @PreAuthorize("hasAnyAuthority('SYS_DICT_UPDATE')")
    @SysLog(msg = "修改字典",type = DicConstants.OperLogType.UPDATE)
    public JsonResult updateById(@RequestBody @Validated SysDict sysDict){
        iSysDictService.edit(sysDict);
        return JsonResult.success();
    }



    @ApiOperation(value = "查询码表")
    @GetMapping("/getItem/{code}")
    @SysLog(msg = "查询码表",type = DicConstants.OperLogType.QUERY)
    public JsonResult getItem(@PathVariable String code){
        return JsonResult.success(iSysDictService.getItem(code));
    }
}
