package com.zwzc.module.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zwzc.module.sys.entity.SysDict;
import com.zwzc.module.sys.entity.SysDictItem;

import java.util.Collection;
import java.util.List;

/**
 * <p>
 * 字典表 Mapper 接口
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-19
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

    List<SysDictItem> getItem(String code);
}
