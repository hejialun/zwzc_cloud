package com.zwzc.module.sys.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;


import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.zwzc.CommMethod;
import com.zwzc.RedisConstants;
import com.zwzc.RedisUtil;
import com.zwzc.module.sys.entity.SysDict;
import com.zwzc.module.sys.entity.SysDictItem;
import com.zwzc.module.sys.mapper.SysDictItemMapper;
import com.zwzc.module.sys.service.ISysDictItemService;
import com.zwzc.module.sys.service.ISysDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 * 字典项表 服务实现类
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-19
 */
@Service
public class SysDictItemServiceImpl extends ServiceImpl<SysDictItemMapper, SysDictItem> implements ISysDictItemService {
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private ISysDictService sysDictService;

    @Override
    public List<SysDictItem> findList(SysDictItem en) {
        QueryWrapper<SysDictItem> qw = new QueryWrapper<>();
        if (StrUtil.isNotEmpty(en.getDictId())) {
            qw.eq("dict_id", en.getDictId());
        }
        qw.orderByAsc("sort");
        return baseMapper.selectList(qw);
    }

    @Override
    @Transactional
    public void delById(String id) {
        //删除字典项
        baseMapper.deleteById(id);
        //获取缓存中的数据
        Map<String, Object> map = (Map<String, Object>) redisUtil.get(RedisConstants.DICT_LIST);
        //删除缓存
        for (String key : map.keySet()) {
            List<SysDictItem> sysDictItems=  JSON.parseArray(JSON.toJSONString(map.get(key))).toJavaList(SysDictItem.class);
            //移除
            map.put(key, sysDictItems.stream().filter(x -> !x.getId().equals(id)).collect(Collectors.toList()));
        }
        //重新缓存
        redisUtil.set(RedisConstants.DICT_LIST, map);
    }

    @Override
    @Transactional
    public void add(SysDictItem sysDictItem) {
        CommMethod.beanCreate(sysDictItem);
        this.save(sysDictItem);
        //获取缓存中的数据
        Map<String, List<SysDictItem>> map = (Map<String, List<SysDictItem>>) redisUtil.get(RedisConstants.DICT_LIST);
        if (ObjectUtil.isEmpty(map)) {
            //全量缓存
            sysDictService.fullCacheRedis();
        } else {
            //查询出当前字典
            SysDict sysDict = sysDictService.getById(sysDictItem.getDictId());
            //获取缓存中已有的字典值
            List<SysDictItem> sysDictItems = map.get(sysDict.getDictCode());
            if (ObjectUtil.isEmpty(sysDictItems)) {
                //第一次缓存
                map.put(sysDict.getDictCode(), new ArrayList<SysDictItem>() {{
                    add(sysDictItem);
                }});
            } else {
                //已有缓存
                map.get(sysDict.getDictCode()).add(sysDictItem);
            }
            //重新缓存
            redisUtil.set(RedisConstants.DICT_LIST, map);
        }


    }

    @Override
    @Transactional
    public void edit(SysDictItem sysDictItem) {
        CommMethod.beanUpdate(sysDictItem);
        this.updateById(sysDictItem);
        //获取缓存中的数据
        Map<String, Object> map = (Map<String, Object>) redisUtil.get(RedisConstants.DICT_LIST);
        //修改缓存
        for (String key : map.keySet()) {
            List<SysDictItem> sysDictItems=  JSON.parseArray(JSON.toJSONString(map.get(key))).toJavaList(SysDictItem.class);
            for (int i = 0; i < sysDictItems.size(); i++) {
                SysDictItem temp = sysDictItems.get(i);
                if (temp.getId().equals(sysDictItem.getId())) {
                    //找到了
                    sysDictItems.set(i, sysDictItem);
                    map.put(key,sysDictItems);
                    break;
                }
            }
        }
        //重新缓存
        redisUtil.set(RedisConstants.DICT_LIST, map);
    }
}
