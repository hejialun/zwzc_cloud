package com.zwzc.module.sys.service;

import com.zwzc.module.common.Pager;
import com.zwzc.module.sys.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;


import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Auto-generator
 * @since 2021-04-27
 */
public interface ISysRoleService extends IService<SysRole> {

    /*
     * @param pager:分页参数
     * @param en：实体对象
     * @Author hejialun
     * @Description: TODO()
     * @date 2021/5/15 1:23
     * @returns com.zwzc.util.Pager<com.zwzc.module.sys.entity.SysRole>
     */
    Pager<SysRole> findPage(Pager<SysRole> pager, SysRole en);

    /**
     *
     * @param userId
     * @Author hejialun
     * @Description: TODO(通过用户查询角色)
     * @Return java.util.List<com.zwzc.module.admin.entity.SysRole>
     * @Date 2021/4/27 20:59
     */
    List<SysRole> findByUserId(String userId);


    /*
     * @param sysRole:角色对象
     * @Author hejialun
     * @Description: TODO(给角色设置菜单)
     * @date 2021/5/15 14:07
     * @returns void
     */
    void setMenuByRole(SysRole sysRole);
    /*
     * @param en
     * @Author hejialun
     * @Description: TODO(查询所有角色)
     * @date 2021/5/15 20:30
     * @returns java.util.List<com.zwzc.module.sys.entity.SysRole>
     */
    List<SysRole> findList(SysRole en);


    /**
     * @Author hejialun
     * @Date 15:09 2022/4/20
     * @Description TODO(新增角色)
     * @param sysRole
     * @return void
     */
    void add(SysRole sysRole);

    /**
     * @Author hejialun
     * @Date 15:13 2022/4/20
     * @Description TODO(修改角色)
     * @param sysRole
     * @return void
     */
    void editById(SysRole sysRole);
}
