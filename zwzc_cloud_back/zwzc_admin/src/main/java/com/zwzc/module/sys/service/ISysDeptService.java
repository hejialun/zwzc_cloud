package com.zwzc.module.sys.service;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zwzc.module.sys.entity.SysDept;

import java.util.List;

/**
 * <p>
 * 部门表 服务类
 * </p>
 *
 * @author hejialun
 * @since 2022-04-18
 */
public interface ISysDeptService extends IService<SysDept> {

    /**
     * @Author hejialun
     * @Date 15:52 2022/4/20
     * @Description TODO(查询当前部门的所有子集部门)
     * @param id
     * @return java.util.List<com.zwzc.module.sys.entity.SysDept>
     */
    List<Tree<String>> findSubsetById(String id);


    /**
     * @Author hejialun
     * @Date 15:49 2022/4/20
     * @Description TODO(查询部门树)
     * @param en
     * @return java.util.List<cn.hutool.core.lang.tree.Tree<java.lang.String>>
     */
    List<Tree<String>> findTree(SysDept en);


    /**
     * @Author hejialun
     * @Date 9:39 2022/4/24
     * @Description TODO(获取部门树并挈带数据权限)
     * @param en
     * @return java.util.List<cn.hutool.core.lang.tree.Tree<java.lang.String>>
     */
    List<Tree<String>> findTreeAuth(SysDept en);

    /**
     * @Author hejialun
     * @Date 15:48 2022/4/20
     * @Description TODO(删除部门)
     * @param id
     * @return void
     */
    void delById(String id);


    /**
     * @Author hejialun
     * @Date 15:49 2022/4/20
     * @Description TODO(新增部门)
     * @param en
     * @return void
     */
    void add(SysDept en);

    /**
     * @Author hejialun
     * @Date 15:49 2022/4/20
     * @Description TODO(修改部门)
     * @param en
     * @return void
     */
    void editById(SysDept en);


}
