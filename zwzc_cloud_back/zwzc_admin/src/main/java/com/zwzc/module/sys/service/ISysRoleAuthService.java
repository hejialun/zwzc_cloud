package com.zwzc.module.sys.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.zwzc.module.sys.entity.SysRoleAuth;
import com.zwzc.module.sys.vo.SysRoleAuth.UserAuthVo;

import java.util.List;
import java.util.Set;

/**
 * <p>
 * 角色自定义权限表 服务类
 * </p>
 *
 * @author hejialun
 * @since 2022-04-20
 */
public interface ISysRoleAuthService extends IService<SysRoleAuth> {


    /**
     * @Author hejialun
     * @Date 15:23 2022/4/20
     * @Description TODO(通过角色ID获取角色自定义权限)
     * @param roleId
     * @return java.util.List<com.zwzc.module.sys.entity.SysRoleAuth>
     */
    List<SysRoleAuth> findCustomByRoleId(String roleId);

    /**
     * @Author hejialun
     * @Date 16:06 2022/4/20
     * @Description TODO(获取用户的数据权限)
     * @param id
     * @return com.zwzc.module.sys.vo.SysRoleAuth.UserAuthVo
     */
    UserAuthVo findByUserId(String id);



}
