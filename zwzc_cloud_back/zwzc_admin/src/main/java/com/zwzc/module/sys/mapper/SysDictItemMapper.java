package com.zwzc.module.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zwzc.module.sys.entity.SysDictItem;

/**
 * <p>
 * 字典项表 Mapper 接口
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-19
 */
public interface SysDictItemMapper extends BaseMapper<SysDictItem> {

}
