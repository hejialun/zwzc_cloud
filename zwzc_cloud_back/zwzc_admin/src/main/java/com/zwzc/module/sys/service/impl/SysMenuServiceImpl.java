package com.zwzc.module.sys.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zwzc.HtException;
import com.zwzc.module.sys.entity.SysMenuRole;
import com.zwzc.module.sys.entity.SysMenu;
import com.zwzc.module.sys.mapper.SysMenuRoleMapper;
import com.zwzc.module.sys.mapper.SysMenuMapper;
import com.zwzc.module.sys.service.ISysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-13
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {
    @Autowired
    private SysMenuRoleMapper menuRoleMapper;

    @Override
    public List<SysMenu> findTreeByUid(String uid) {
        //查询当前用户所拥有的的菜单
        List<SysMenu> sysMenus=baseMapper.findByUserId(uid);
        //构建数
        return buildTree(sysMenus,"0");
    }

    @Override

    public List<SysMenu> findMenuTree() {
        return buildTree(baseMapper.selectList(new QueryWrapper<SysMenu>().orderByAsc("sort")),"0");
    }

    @Override
    public List<SysMenuRole> findMenuRole(String rid) {
        //删除原来的所有菜单
        QueryWrapper<SysMenuRole> qw = new QueryWrapper<>();
        qw.eq("role_id", rid);
        return menuRoleMapper.selectList(qw);
    }

    @Override
    public void delById(String id) {
        //判断当前菜单是否可以删除
        if(baseMapper.selectCount(new QueryWrapper<SysMenu>().eq("pid",id))>0){
            throw new HtException("当前菜单下还有子项不可删除！");
        }else{
            //删除
            baseMapper.deleteById(id);
        }

    }


    /**
     * 构建才菜单树
     * @return
     */
    @Override
    public List<SysMenu> buildTree(List<SysMenu> menuList,String pid){
        List<SysMenu> children = new ArrayList<>();
        menuList.forEach(menu -> {
            if (menu.getPid().equals(pid)) {
                menu.setChildren(buildTree(menuList, menu.getId()));
                children.add(menu);
            }
        });
        return children;
    }




}
