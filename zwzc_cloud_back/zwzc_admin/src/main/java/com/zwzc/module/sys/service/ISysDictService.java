package com.zwzc.module.sys.service;

import com.zwzc.module.common.Pager;
import com.zwzc.module.sys.entity.SysDict;
import com.baomidou.mybatisplus.extension.service.IService;
import com.zwzc.module.sys.entity.SysDictItem;


import java.util.List;
import java.util.Map;

/**
 * <p>
 * 字典表 服务类
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-19
 */
public interface ISysDictService extends IService<SysDict> {

    Pager<SysDict> findPage(Pager<SysDict> pager, SysDict sysDict);

    List<SysDictItem> getItem(String code);

    /*
     * @param
     * @Author hejialun
     * @Description: TODO(全量同步到redis)
     * @date 2021/9/17 17:20
     * @returns void
     */
    void fullCacheRedis();

    /*
     * @param map:redis缓存中的数据
     * @param code：需要同步的字典编码
     * @Author hejialun
     * @Description: TODO(增量同步)
     * @date 2021/9/17 17:21
     * @returns java.util.List<com.zwzc.module.sys.entity.SysDictItem>
     */
    List<SysDictItem> addCacheRedis(Map<String, List<SysDictItem>> map, String code);

    /*
     * @param sysDict
     * @Author hejialun
     * @Description: TODO(修改字典)
     * @date 2021/9/18 16:03
     * @returns void
     */
    void edit(SysDict sysDict);

    /*
     * @param id
     * @Author hejialun
     * @Description: TODO(删除字典表)
     * @date 2021/9/18 16:09
     * @returns void
     */
    void delById(String id);
}
