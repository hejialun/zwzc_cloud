package com.zwzc.module.sys.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zwzc.dataAuth.DataScope;
import com.zwzc.module.sys.entity.SysDept;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 部门表 Mapper 接口
 * </p>
 *
 * @author hejialun
 * @since 2022-04-18
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

    List<SysDept> findList(@Param(Constants.WRAPPER) QueryWrapper<SysDept> qw);


    List<SysDept> findListAuth(@Param(Constants.WRAPPER) QueryWrapper<SysDept> qw, DataScope dataScope);
}
