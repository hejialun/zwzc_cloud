package com.zwzc.module.sys.controller;


import com.zwzc.DicConstants;
import com.zwzc.log.annotation.SysLog;
import com.zwzc.module.common.JsonResult;
import com.zwzc.module.sys.entity.SysDictItem;
import com.zwzc.module.sys.service.ISysDictItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.util.CollectionUtils;
import java.util.*;


/**
 * <p>
 * 字典项表 前端控制器
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-19
 */
@Api(tags = "字典项表 ")
@RestController
@RequestMapping("/sys-dict-item")
public class SysDictItemController {

    
    @Autowired
    private ISysDictItemService iSysDictItemService;

    



    @ApiOperation(value = "列表查询")
    @GetMapping("/findList")
    @SysLog(msg = "列表查询字典项")
    public JsonResult findList(SysDictItem en){
        return JsonResult.success(iSysDictItemService.findList(en));
    }



    @ApiOperation(value = "新增")
    @PostMapping("/add")
    @PreAuthorize("hasAnyAuthority('SYS_DICT_ITEM_ADD')")
    @SysLog(msg = "新增字典项",type = DicConstants.OperLogType.ADD)
    public JsonResult add(@RequestBody  @Validated SysDictItem sysDictItem){
        iSysDictItemService.add(sysDictItem);
        return JsonResult.success();
    }

    @DeleteMapping("/delete-by-id/{id}")
    @ApiOperation(value = "删除")
    @PreAuthorize("hasAnyAuthority('SYS_DICT_ITEM_DEL')")
    @SysLog(msg = "删除字典项",type = DicConstants.OperLogType.DEL)
    public JsonResult delete(@PathVariable(value = "id") String id){
        iSysDictItemService.delById(id);
        return JsonResult.success();
    }


    @PutMapping("/update")
    @ApiOperation(value = "修改")
    @PreAuthorize("hasAnyAuthority('SYS_DICT_ITEM_UPDATE')")
    @SysLog(msg = "修改字典项",type = DicConstants.OperLogType.UPDATE)
    public JsonResult updateById(@RequestBody @Validated SysDictItem sysDictItem){
        iSysDictItemService.edit(sysDictItem);
        return JsonResult.success();
    }

}
