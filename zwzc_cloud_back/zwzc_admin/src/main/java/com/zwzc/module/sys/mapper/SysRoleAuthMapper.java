package com.zwzc.module.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zwzc.module.sys.entity.SysRoleAuth;

/**
 * <p>
 * 角色自定义权限表 Mapper 接口
 * </p>
 *
 * @author hejialun
 * @since 2022-04-20
 */
public interface SysRoleAuthMapper extends BaseMapper<SysRoleAuth> {

}
