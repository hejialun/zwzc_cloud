package com.zwzc.module.sys.service;

import com.zwzc.module.sys.entity.SysDictItem;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 字典项表 服务类
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-19
 */
public interface ISysDictItemService extends IService<SysDictItem> {

    List<SysDictItem> findList(SysDictItem en);

    void delById(String id);

    void add(SysDictItem sysDictItem);

    void edit(SysDictItem sysDictItem);
}
