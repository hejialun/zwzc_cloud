package com.zwzc.module.sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zwzc.module.sys.entity.SysRole;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Auto-generator
 * @since 2021-04-27
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

    List<SysRole> findByUserId(String userId);
}
