package com.zwzc.module.login.controller;

import com.zwzc.BusConstant;
import com.zwzc.RedisConstants;
import com.zwzc.RedisUtil;
import com.zwzc.module.common.JsonResult;
import com.wf.captcha.ArithmeticCaptcha;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ProjectName: zwzc
 * @ClassName: LoginController
 * @Author: hejialun
 * @Description: 登录相关接口
 * @Date: 2021/5/18 23:08
 */
@RestController
@RequestMapping("/sys-login")
@Api(tags = "登录相关接口 ")
public class LoginController {
    @Autowired
    private RedisUtil redisUtil;

    /*
     * @param randomStr:随机字符串
     * @Author hejialun
     * @Description: TODO(创建验证码)
     * @date 2021/5/25 9:55
     * @returns com.zwzc.util.JsonResult
     */
    @GetMapping(value = "/getCode")
    public JsonResult getCode(String randomStr) throws Exception {
        ArithmeticCaptcha captcha = new ArithmeticCaptcha(111, 36);
        // 几位数运算，默认是两位
        captcha.setLen(2);
        // 获取运算的结果
        String result = captcha.text();
        // 保存
        redisUtil.set(RedisConstants.VERIFICATION_CODE_PREFIX+randomStr,result,BusConstant.VERIFICATION_CODE_OVERDUE_TIME);
        return JsonResult.success(captcha.toBase64());

    }
}