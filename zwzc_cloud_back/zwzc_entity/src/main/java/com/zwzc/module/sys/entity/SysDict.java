package com.zwzc.module.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * <p>
 * 字典表
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-19
 */

@TableName("sys_dict")
@Data
public class SysDict implements  Serializable {


    @ApiModelProperty("主键")
    @TableId
    private String id;

    @ApiModelProperty("字典名称")
    @NotBlank
    private String dictName;

    @ApiModelProperty("字典编码")
    @NotBlank
    private String dictCode;

    @ApiModelProperty("描述")
    @TableField(value = "`desc`")
    private String desc;


    @ApiModelProperty("创建时间")
    private LocalDateTime createDate;

    @ApiModelProperty("创建人")
    private String createUser;


    @ApiModelProperty("修改时间")
    private LocalDateTime updateDate;


    @ApiModelProperty("修改人")
    private String updateUser;
}
