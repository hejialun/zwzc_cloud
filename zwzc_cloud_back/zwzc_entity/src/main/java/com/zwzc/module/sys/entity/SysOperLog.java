package com.zwzc.module.sys.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.experimental.Accessors;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import java.io.Serializable;


/**
 * <p>
 * 系统操作日志
 * </p>
 *
 * @author hejialun
 * @since 2022-04-26
 */

@TableName("sys_oper_log")
@Data
@Accessors(chain = true)
public class SysOperLog{

	@ApiModelProperty("日志ID")
    @TableId
    private String id;

	@ApiModelProperty("模块标题")
    private String title;

	@ApiModelProperty("操作类型（关联码表：log_oper_type）")
    private String operType;

	@ApiModelProperty("方法名称")
    private String methodName;

	@ApiModelProperty("请求方式")
    private String requestMethod;

	@ApiModelProperty("操作用户")
    private String userId;

	@ApiModelProperty("请求URL")
    private String operUrl;

	@ApiModelProperty("主机地址")
    private String operIp;

	@ApiModelProperty("请求参数")
    private String param;

	@ApiModelProperty("操作说明")
    private String msg;

	@ApiModelProperty("操作状态（0正常1异常）")
    private String states;

	@ApiModelProperty("错误信息")
    private String errMsg;

	@ApiModelProperty("操作时间")
    private LocalDateTime operTime;

    @ApiModelProperty("操作用户姓名")
    @TableField(exist = false)
	private String userName;

}
