package com.zwzc.module.sys.entity;

        import com.baomidou.mybatisplus.annotation.TableField;
        import com.baomidou.mybatisplus.annotation.TableId;
        import com.baomidou.mybatisplus.annotation.TableLogic;
        import com.baomidou.mybatisplus.annotation.TableName;
        import com.baomidou.mybatisplus.extension.activerecord.Model;
        import io.swagger.annotations.ApiModelProperty;
        import lombok.Data;

        import javax.validation.constraints.NotBlank;
        import java.io.Serializable;


/**
 * <p>
 * 字典项表
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-19
 */

@TableName("sys_dict_item")
@Data
public class SysDictItem implements Serializable{
        @ApiModelProperty("主键")
        @TableId
        private String id;

        @ApiModelProperty("字典id")
        @NotBlank
        private String dictId;

        @ApiModelProperty("字典项文本")
        @NotBlank
        private String label;

        @ApiModelProperty("字典项值")
        @NotBlank
        private String value;

        @ApiModelProperty("描述")
        @TableField(value = "`desc`")
        private String desc;

        @ApiModelProperty("排序")
        private Integer sort;
}
