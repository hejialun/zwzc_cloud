package com.zwzc.module.sys.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * <p>
 * 权限菜单表
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-13
 */

@TableName("sys_menu_role")
@Data
public class SysMenuRole {



    @ApiModelProperty("主键")
    @TableId
    private String id;

    @ApiModelProperty("权限id")
    private String roleId;


    @ApiModelProperty("菜单id")
    private String menuId;



}
