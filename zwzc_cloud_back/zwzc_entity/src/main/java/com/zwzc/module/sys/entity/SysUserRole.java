package com.zwzc.module.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * <p>
 * 
 * </p>
 *
 * @author Auto-generator
 * @since 2021-04-27
 */

@TableName("sys_user_role")
@Data
public class SysUserRole {

    @ApiModelProperty("用户角色表")
    @TableId
    private String id;

    @ApiModelProperty("角色id")
    private String roleId;

    @ApiModelProperty("用户id")
    private String userId;


}
