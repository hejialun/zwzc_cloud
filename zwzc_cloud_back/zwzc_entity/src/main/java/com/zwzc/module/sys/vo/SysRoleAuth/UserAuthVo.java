package com.zwzc.module.sys.vo.SysRoleAuth;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Set;

/**
 * @ClassName UserAuthVo
 * @Description TODO（用户权限vo）
 * @Author hejialun
 * @Date 2022/4/20 15:41
 * @Version 1.0
 */
@Data
@Accessors(chain = true)
public class UserAuthVo {

    @ApiModelProperty("部门数据权限")
    private Set<String> deptIds;

    @ApiModelProperty("用户数据权限-用户仅限本人数据权限")
    private Set<String> user;
}
