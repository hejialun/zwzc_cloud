package com.zwzc.module.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;


/**
 * <p>
 * 部门表
 * </p>
 *
 * @author hejialun
 * @since 2022-04-18
 */

@TableName("sys_dept")
@Data
@Accessors(chain = true)
public class SysDept{

    @ApiModelProperty("部门id")
    @TableId
    private String deptId;

    @ApiModelProperty("上级部门id")
    private String parentId;

    @ApiModelProperty("部门名称")
    @NotEmpty
    private String deptName;

    @ApiModelProperty("部门编码")
    @NotEmpty
    private String deptCode;


    @ApiModelProperty("排序")
    @NotNull
    private Integer sort;


    @ApiModelProperty("逻辑删除")
    @TableLogic
    private Integer delFlag;


    @ApiModelProperty("创建时间")
    private LocalDateTime createDate;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("修改时间")
    private LocalDateTime updateDate;

    @ApiModelProperty("修改人")
    private String updateUser;


    @ApiModelProperty("创建人名")
    @TableField(exist = false)
    private String createUserName;
}
