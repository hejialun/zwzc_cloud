package com.zwzc.module.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;


/**
 * <p>
 * 
 * </p>
 *
 * @author Auto-generator
 * @since 2021-04-27
 */

@TableName("sys_user")
@Data
@Accessors(chain = true)
public class SysUser implements  Serializable{

    @TableId
    private String id;

    @ApiModelProperty("用户名")
    @NotBlank
    private String username;

    @ApiModelProperty("密码")
    private String password;

    @ApiModelProperty("昵称")
    @NotBlank
    private String name;

    @ApiModelProperty("所属部门")
    @NotBlank
    private String deptId;

    @ApiModelProperty("字段状态：01:启用：02：禁用  -关联码表state")
    @NotBlank
    private String state;

    
    @ApiModelProperty("创建时间")
    private LocalDateTime createDate;
    
    @ApiModelProperty("创建人")
    private String createUser;

    
    @ApiModelProperty("修改时间")
    private LocalDateTime updateDate;

    
    @ApiModelProperty("修改人")
    private String updateUser;

    
    @ApiModelProperty("角色名拼接字符串")
    @TableField(exist = false)
    private String roleNames;

    
    @ApiModelProperty("用户菜单")
    @TableField(exist = false)
    private List<SysMenu> sysMenus;


    
    @ApiModelProperty("用户菜单不是树")
    @TableField(exist = false)
    private List<SysMenu> sysMenusNoTree;

    
    @ApiModelProperty("用户按钮权限")
    @TableField(exist = false)
    private List<SysMenu> permissions;

    
    @ApiModelProperty("用户角色id集合")
    @TableField(exist = false)
    private List<String> roles;

    
    @ApiModelProperty("用户角色集合")
    @TableField(exist = false)
    private List<SysRole> roleObjs;

    
    @ApiModelProperty("用户创建月份")
    @TableField(exist = false)
    private Integer createMonth;

    
    @ApiModelProperty("部门名称")
    @TableField(exist = false)
    private String deptName;

    @ApiModelProperty("用户编码集合")
    @TableField(exist = false)
    private String roleCodes;

    @ApiModelProperty("用户id集合")
    @TableField(exist = false)
    private String roleIds;


}
