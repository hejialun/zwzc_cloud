package com.zwzc.module.sys.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotEmpty;

/**
 * @ProjectName: results_comm
 * @ClassName: UpdatePasswordDto
 * @Author: hejialun
 * @Description: 修改密码dto
 * @Date: 2022/1/20 15:56
 */
@Data
@Accessors(chain = true)
public class UpdatePasswordDto {
    @NotEmpty
    private String oldPassword;
    @NotEmpty
    private String password;
    @NotEmpty
    private String confirmPassword;
}