package com.zwzc.module.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>
 * 
 * </p>
 *
 * @author Auto-generator
 * @since 2021-04-27
 */

@TableName("sys_role")
@Data
public class SysRole {

    @ApiModelProperty("角色表主键")
    @TableId
    private String id;

    @ApiModelProperty("角色编码")
    @NotEmpty
    private String code;

    @ApiModelProperty("角色名")
    @NotEmpty
    private String name;

    @ApiModelProperty("角色权限标识-关联字典表（data_authority）")
    @NotEmpty
    private String dataAuthority;

    @ApiModelProperty("创建时间")
    private LocalDateTime createDate;

    @ApiModelProperty("创建人")
    private String createUser;

    @ApiModelProperty("修改时间")
    private LocalDateTime updateDate;

    @ApiModelProperty("修改时间")
    private String updateUser;

    @ApiModelProperty("菜单集合")
    @TableField(exist = false)
    private List<SysMenu> menus = new ArrayList<>();

    @ApiModelProperty("数据权限部门集合")
    @TableField(exist = false)
    private List<String> custom=new ArrayList<>();
}
