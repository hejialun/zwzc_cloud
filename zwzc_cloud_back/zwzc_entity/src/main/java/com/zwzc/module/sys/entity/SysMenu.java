package com.zwzc.module.sys.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;


/**
 * <p>
 * 菜单表
 * </p>
 *
 * @author Auto-generator
 * @since 2021-05-13
 */

@TableName("sys_menu")
@Data
public class SysMenu implements  Serializable{

    @ApiModelProperty("菜单id")
    @TableId
    private String id;
    
    @ApiModelProperty("上级菜单")
    private String pid;
    
    @ApiModelProperty("菜单名")
    @NotBlank
    private String name;
    
    @ApiModelProperty("编码")
    @NotBlank
    private String code;
    
    @ApiModelProperty("图标")
    private String icon;
    
    @ApiModelProperty("菜单路径")
    private String path;
    
    @ApiModelProperty("菜单类型：0：菜单：1：页面：2：按钮")
    @NotBlank
    private String type;
    
    @ApiModelProperty("排序")
    private Integer sort;

    @ApiModelProperty("是否开启缓存：关联码表：whether")
    private String cache;

    
    @ApiModelProperty("菜单子项")
    @TableField(exist = false)
    private List<SysMenu> children;


}
