package com.zwzc.module.sys.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;


/**
 * <p>
 * 角色自定义权限表
 * </p>
 *
 * @author hejialun
 * @since 2022-04-20
 */

@TableName("sys_role_auth")
@Data
@Accessors(chain = true)
public class SysRoleAuth{

	@ApiModelProperty("角色自定义权限表")
    @TableId
    private String id;

	@ApiModelProperty("角色id")
    private String roleId;

	@ApiModelProperty("部门id")
    private String deptId;


}
