package com.zwzc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @ClassName GateWayApplication
 * @Description TODO（网关路由）
 * @Author hejialun
 * @Date 2022/5/5 14:46
 * @Version 1.0
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableDiscoveryClient
public class GateWayApplication{
    public static void main(String[] args) {
        SpringApplication.run(GateWayApplication.class, args);
    }
}
