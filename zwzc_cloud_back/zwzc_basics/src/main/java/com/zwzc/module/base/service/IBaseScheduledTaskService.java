package com.zwzc.module.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zwzc.module.base.entity.BaseScheduledTask;
import com.zwzc.module.common.Pager;
import org.springframework.scheduling.config.ScheduledTask;

import java.util.List;

/**
 * <p>
 * 定时任务表 服务类
 * </p>
 *
 * @author hejialun
 * @since 2022-04-24
 */
public interface IBaseScheduledTaskService extends IService<BaseScheduledTask> {

    /**
     * @Author hejialun
     * @Date 10:31 2022/4/26
     * @Description TODO(分页查询)
     * @param pager
     * @param en
     * @return com.ht.util.Pager<com.ht.module.base.entity.BaseScheduledTask>
     */
    Pager<BaseScheduledTask> findPage(Pager<BaseScheduledTask> pager, BaseScheduledTask en);



    /**
     * @Author hejialun
     * @Date 16:07 2022/4/25
     * @Description TODO(查询所有：生成定时任务所需)
     * @param
     * @return java.util.List<com.ht.module.base.entity.BaseScheduledTask>
     */
    List<BaseScheduledTask> listAll();


    /**
     * @Author hejialun
     * @Date 16:07 2022/4/25
     * @Description TODO(根据id查询)
     * @param id
     * @return org.springframework.scheduling.config.ScheduledTask
     */
    BaseScheduledTask getById(String id);


    /**
     * @Author hejialun
     * @Date 16:07 2022/4/25
     * @Description TODO(根据完整类名查询)
     * @param taskClass
     * @return org.springframework.scheduling.config.ScheduledTask
     */
    BaseScheduledTask findByTaskClass(String taskClass);

    /**
     * @Author hejialun
     * @Date 16:07 2022/4/25
     * @Description TODO(执行定时任务)
     * @param id
     * @return void
     */
    void start(String id);


    /**
     * @Author hejialun
     * @Date 16:08 2022/4/25
     * @Description TODO(停止定时任务)
     * @param id
     * @return void
     */
    void stop(String id);


    /**
     * @Author hejialun
     * @Date 16:08 2022/4/25
     * @Description TODO(重启定时任务)
     * @param id
     * @return void
     */
     void restart(String id);

    /**
     * @Author hejialun
     * @Date 16:09 2022/4/25
     * @Description TODO(初始化定时任务)
     * @param
     * @return void
     */
    void initTask();


    /**
     * @Author hejialun
     * @Date  2022/4/26
     * @Description TODO(删除定时任务)
     * @param id
     * @return void
     */
    void delById(String id);

    /**
     * @Author hejialun
     * @Date 11:11 2022/4/26
     * @Description TODO(新增任务)
     * @param en
     * @return void
     */
    void add(BaseScheduledTask en);

    /**
     * @Author hejialun
     * @Date 11:13 2022/4/26
     * @Description TODO(修改任务)
     * @param en
     * @return void
     */
    void editById(BaseScheduledTask en);
}
