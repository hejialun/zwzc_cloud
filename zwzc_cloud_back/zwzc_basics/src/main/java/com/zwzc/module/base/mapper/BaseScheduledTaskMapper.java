package com.zwzc.module.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zwzc.module.base.entity.BaseScheduledTask;

/**
 * <p>
 * 定时任务表 Mapper 接口
 * </p>
 *
 * @author hejialun
 * @since 2022-04-24
 */
public interface BaseScheduledTaskMapper extends BaseMapper<BaseScheduledTask> {

}
