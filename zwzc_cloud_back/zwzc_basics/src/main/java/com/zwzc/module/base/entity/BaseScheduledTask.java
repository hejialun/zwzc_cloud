package com.zwzc.module.base.entity;

import lombok.Data;
import lombok.experimental.Accessors;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;
import java.io.Serializable;


/**
 * <p>
 * 定时任务表
 * </p>
 *
 * @author hejialun
 * @since 2022-04-24
 */

@TableName("base_scheduled_task")
@Data
@Accessors(chain = true)
public class BaseScheduledTask{

	@ApiModelProperty("定时任务id")
    @TableId
    private String id;

	@ApiModelProperty("任务名")
    @NotEmpty
    private String taskName;

	@ApiModelProperty("后台任务：关联码表(back_task)")
    @NotEmpty
    private String backTask;

	@ApiModelProperty("corn表达式")
    @NotEmpty
    private String corn;

	@ApiModelProperty("任务状态:关联码表(task_state)")
    private String state;

	@ApiModelProperty("任务备注")
    private String note;

	@ApiModelProperty("创建时间")
    private LocalDateTime createDate;

	@ApiModelProperty("创建人")
    private String createUser;

	@ApiModelProperty("修改时间")
    private LocalDateTime updateDate;

	@ApiModelProperty("修改人")
    private String updateUser;


}
