package com.zwzc.module.base.controller;



import com.zwzc.DicConstants;
import com.zwzc.log.annotation.SysLog;
import com.zwzc.module.base.entity.BaseScheduledTask;
import com.zwzc.module.base.service.IBaseScheduledTaskService;
import com.zwzc.module.common.JsonResult;
import com.zwzc.module.common.Pager;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * <p>
 * 定时任务表 前端控制器
 * </p>
 *
 * @author hejialun
 * @since 2022-04-24
 */
@RestController
@RequestMapping("/base-scheduled-task")
public class BaseScheduledTaskController {

    
    @Autowired
    private IBaseScheduledTaskService iBaseScheduledTaskService;

    @ApiOperation(value = "激活任务")
    @PostMapping("/start/{id}")
    @PreAuthorize("hasAnyAuthority('BASE_SCHEDULED_TASK_START')")
    @SysLog(msg = "激活定时任务",type = DicConstants.OperLogType.ACTIVE)
    @Transactional
    public JsonResult start(@PathVariable String id){
        iBaseScheduledTaskService.start(id);
        //修改字段
        iBaseScheduledTaskService.updateById(new BaseScheduledTask().setId(id).setState(DicConstants.TaskState.ACTIVE));
        return JsonResult.success();
    }

    @ApiOperation(value = "停止定时任务")
    @PostMapping("/stop/{id}")
    @PreAuthorize("hasAnyAuthority('BASE_SCHEDULED_TASK_STOP')")
    @SysLog(msg = "激活定时任务",type = DicConstants.OperLogType.STOP)
    @Transactional
    public JsonResult stop(@PathVariable String id){
        iBaseScheduledTaskService.stop(id);
        //修改字段
        iBaseScheduledTaskService.updateById(new BaseScheduledTask().setId(id).setState(DicConstants.TaskState.STOP));
        return JsonResult.success();
    }

    @ApiOperation(value = "分页查询")
    @GetMapping("/findPage")
    @PreAuthorize("hasAnyAuthority('BASE_SCHEDULED_TASK')")
    @SysLog(msg = "分页查询定时任务")
    public JsonResult findPage(Pager<BaseScheduledTask> pager, BaseScheduledTask en){
        return JsonResult.success(iBaseScheduledTaskService.findPage(pager,en));
    }

    @ApiOperation(value = "新增")
    @PostMapping("/add")
    @PreAuthorize("hasAnyAuthority('BASE_SCHEDULED_TASK_ADD')")
    @SysLog(msg = "新增定时任务",type = DicConstants.OperLogType.ADD)
    public JsonResult add(@RequestBody @Valid BaseScheduledTask en){
        iBaseScheduledTaskService.add(en);
        return JsonResult.success();
    }

    @ApiOperation(value = "通过id删除")
    @DeleteMapping("/delete-by-id/{id}")
    @PreAuthorize("hasAnyAuthority('BASE_SCHEDULED_TASK_DEL')")
    @SysLog(msg = "删除定时任务",type = DicConstants.OperLogType.DEL)
    public JsonResult delete(@PathVariable(value = "id") String id){
        iBaseScheduledTaskService.delById(id);
        return JsonResult.success();
    }

    @ApiOperation(value = "修改")
    @PutMapping("/update")
    @PreAuthorize("hasAnyAuthority('BASE_SCHEDULED_TASK_UPDATE')")
    @SysLog(msg = "修改定时任务",type = DicConstants.OperLogType.UPDATE)
    public JsonResult updateById(@RequestBody @Valid BaseScheduledTask en){
        iBaseScheduledTaskService.editById(en);
        return JsonResult.success();
    }

}
