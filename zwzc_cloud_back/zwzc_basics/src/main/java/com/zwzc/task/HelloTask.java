package com.zwzc.task;


import com.zwzc.config.task.ScheduledOfTask;
import com.zwzc.module.base.entity.BaseScheduledTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @ClassName HelloTask
 * @Description TODO（问好定时任务-用于测试）
 * @Author hejialun
 * @Date 2022/8/9 15:03
 * @Version 1.0
 */
@Component
@Slf4j
public class HelloTask implements ScheduledOfTask {
    @Override
    public void execute(BaseScheduledTask st) {
        System.out.println("你好啊");
        log.info("----{}----",st.getTaskName());
    }
}
