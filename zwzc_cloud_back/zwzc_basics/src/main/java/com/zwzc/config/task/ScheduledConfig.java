package com.zwzc.config.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Component;

/**
 * @ClassName ScheduledConfig
 * @Description TODO（定时任务线程池配置）
 * @Author hejialun
 * @Date 2022/4/25 16:04
 * @Version 1.0
 */
@Component
@Slf4j
public class ScheduledConfig {


    //初始化定时任务县城次
    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        log.info("创建定时任务调度线程池 start");
        ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
        // 线程池大小
        threadPoolTaskScheduler.setPoolSize(20);
        // 线程名前缀
        threadPoolTaskScheduler.setThreadNamePrefix("taskExecutor-");
        // 停止系统时等待任务完成
        threadPoolTaskScheduler.setWaitForTasksToCompleteOnShutdown(true);
        // 等待停止时间：秒
        threadPoolTaskScheduler.setAwaitTerminationSeconds(60);
        log.info("创建定时任务调度线程池 end");
        return threadPoolTaskScheduler;
    }


}
