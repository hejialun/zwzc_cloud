package com.zwzc.config.task;

import com.zwzc.module.base.service.IBaseScheduledTaskService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * @ClassName ScheduledTaskRunner
 * @Description TODO（初始化定时任务）
 * @Author hejialun
 * @Date 2022/4/25 17:42
 * @Version 1.0
 */
@Slf4j
@Component
public class ScheduledTaskRunner implements ApplicationRunner {
    @Autowired
    private IBaseScheduledTaskService baseScheduledTaskService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("----初始化定时任务开始----");
        baseScheduledTaskService.initTask();
        log.info("----初始化定时任务完成----");
    }
}
