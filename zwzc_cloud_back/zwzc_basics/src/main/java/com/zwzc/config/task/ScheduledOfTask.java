package com.zwzc.config.task;

import cn.hutool.extra.spring.SpringUtil;
import com.zwzc.module.base.entity.BaseScheduledTask;
import com.zwzc.module.base.service.IBaseScheduledTaskService;

/**
 * @ClassName ScheduledOfTask
 * @Description TODO（自定义定时任务接口）
 * @Author hejialun
 * @Date 2022/4/25 16:29
 * @Version 1.0
 */
public interface ScheduledOfTask extends Runnable{
    /**
     * @Author hejialun
     * @Date 9:57 2022/4/26
     * @Description TODO(定时任务方法)
     * @param st:任务实体
     * @return void
     */
    void execute(BaseScheduledTask st);

    /**
     * 实现控制定时任务启用或禁用的功能
     */
    @Override
    default void run() {
        IBaseScheduledTaskService stService = SpringUtil.getBean(IBaseScheduledTaskService.class);
        BaseScheduledTask st = stService.findByTaskClass(this.getClass().getName());
        execute(st);
    }

}
